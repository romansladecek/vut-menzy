//
//  AppDelegate.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  AppDelegate
//

#import "AppDelegate.h"

#import "ZoznamMenzViewController.h"
#import "TutorialViewController.h"
#import "Appirater.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize zoznamMenzViewController = _zoznamMenzViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [Appirater setAppId:@"748246913"];

    
    [self initGlobalVariables];
    
    // set NO after production (not only offline XMLs)
    test_version = NO;
    
    // keys for sorting sections of meals (possible to change  afterwards)
    global_needsSorting = [NSArray arrayWithObjects:@"Polévka", @"Hl. jídlo", @"Ostatní", nil];
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"price_displayed"]) { // if app was started at first time
        [[NSUserDefaults standardUserDefaults] setObject:STUDENT forKey:@"price_displayed"]; // student
    }
    
    // init global variables
    global_menzyDict = [[NSMutableDictionary alloc] init];
    global_otevrenoDict = [[NSMutableDictionary alloc] init];
    global_jidlaDict = [[NSMutableDictionary alloc] init];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.zoznamMenzViewController = [[ZoznamMenzViewController alloc] init];
    self.tutorialViewController = [[TutorialViewController alloc] init];

    
    //UINavigationController* navController1 = [[UINavigationController alloc]
    //                                          initWithRootViewController:self.zoznamMenzViewController];
    
    
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // bcs of easter egg
    application.applicationSupportsShakeToEdit = YES;

    // first time
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"show_tutorial"]) {
        self.window.rootViewController = self.tutorialViewController;
    }
    else {
        self.window.rootViewController = self.zoznamMenzViewController;
    }

    
    [self.window makeKeyAndVisible];
    
    
    [Appirater setDaysUntilPrompt:1];
    [Appirater setUsesUntilPrompt:10];

    
    [Appirater appLaunched:YES];
    
    return YES;
}

-(void)initGlobalVariables {
    
    UIDevice* thisDevice = [UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        isPad = YES;
    }
    else
    {
        isPad = NO;
    }
    
    // measures
    if([UIScreen mainScreen].bounds.size.height == 568)
    { // iphone5 (long screen)
        isLongScreen = YES;
        if (IOS6_VERSION)
        {
            global_ios7top = 0;
        }
        else
        {
            global_ios7top = 20;
        }
    }
    else
    { // iphone4<
        isLongScreen = NO;
        if (IOS6_VERSION)
        {
            global_ios7top = 0;
        }
        else
        {
            global_ios7top = 20;
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
