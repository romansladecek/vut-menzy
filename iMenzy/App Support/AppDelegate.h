//
//  AppDelegate.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  AppDelegate
//

#import <UIKit/UIKit.h>

@class ZoznamMenzViewController, TutorialViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ZoznamMenzViewController *zoznamMenzViewController;
@property (strong, nonatomic) TutorialViewController *tutorialViewController;

@end
