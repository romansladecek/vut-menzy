//
//  LocationManager.h
//
//  License: MIT
//
//  -- Example --
//
//     #import "LocationManager.h"
//
//     // wait up to 120 ms for location
//     CLLocation *here = [LocationManager currentLocationByWaitingUpToMilliseconds:120];
//
//     if (here) {
//        // position available
//     } else {
//        // position unavailable
//     }

#import <Foundation/Foundation.h>

#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
#import <CoreLocation/CoreLocation.h>
#else // OSX
#endif

@interface LocationManager : NSObject <CLLocationManagerDelegate>

// blocks first (indefinitely) if there is not yet an authorization decision
// blocks up to milliseconds, 0 never timeout
// returns nil if no location is found
// compatible with the main thread and runloop
+ (CLLocation *)currentLocationByWaitingUpToMilliseconds:(NSUInteger)milliseconds;

+ (void)start;
+ (void)stop; // turn off the lights when you're done or kill the battery.
+ (BOOL)isStarted;
+ (BOOL)isAuthorized; // YES = location allowed
@end