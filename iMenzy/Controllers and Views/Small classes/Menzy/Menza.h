//
//  Menza.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Menza object
//

#import <Foundation/Foundation.h>

@interface Menza : NSObject {
    
    NSString *idd;
	NSString *title;
	NSString *address;
    int order;
}

@property (nonatomic, retain) NSString *idd;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *address;
@property (assign, readwrite) int order;

-(id)initWithMenza:(NSMutableDictionary *)menza;

@end
