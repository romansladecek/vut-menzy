//
//  Menza.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Menza object
//

#import "Menza.h"

@implementation Menza

@synthesize idd, title, address, order;

-(id)initWithMenza:(NSMutableDictionary *)menza {
    
	self.idd = menza[@"id"];
	self.title = menza[@"nazev"];
	self.address = menza[@"adresa"];
    self.order = [menza[@"poradi"] intValue];
	return self;
}




@end
