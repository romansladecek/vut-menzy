//
//  MenzyCollectionViewCell.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Menzy collection view
//

#import <UIKit/UIKit.h>
#import "Menza.h"

@interface MenzyCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) id parentVC;

@property (nonatomic, retain) Menza *menza;
@property (nonatomic, retain) UILabel *lbl_menza_title;
@property (nonatomic, retain) UIImageView *img_status;
@property (nonatomic, retain) UIImageView *img_bckg;

@property (nonatomic, retain) UIImageView *img_opened;
@property (nonatomic, retain) UILabel *lbl_opened_minutes;


-(void)startJiggling;
-(void)noJiggling;

@end
