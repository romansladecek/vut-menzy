//
//  MenzyCollectionViewCell.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Menzy collection view
//

#import "MenzyCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>

#define degreesToRadians(x) (M_PI * (x) / 180.0)
#define kAnimationRotateDeg 2.0

@implementation MenzyCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        // delete button
        UIImage *delImg = [UIImage imageNamed:@"delete_black.png"];
        self.img_status = [[UIImageView alloc] init];
        self.img_status.frame = CGRectMake(5, 5, 11, 11);
        [self.img_status setImage:delImg];
        
        // opened status
        self.img_opened = [[UIImageView alloc] init];
        self.img_opened.frame = CGRectMake(70, 5, 10, 10);
        self.img_opened.alpha = 0.6;
        self.img_opened.clipsToBounds = YES;
        self.img_opened.layer.cornerRadius = self.img_opened.bounds.size.width / 2;
        
        // label - opened minutes yet
        self.lbl_opened_minutes = [[UILabel alloc] init];
        self.lbl_opened_minutes.frame = CGRectMake(70, 5, 10, 10); // 18
        self.lbl_opened_minutes.numberOfLines = 1;
        self.lbl_opened_minutes.textAlignment = NSTextAlignmentCenter;
        self.lbl_opened_minutes.font = [UIFont fontWithName:DEF_FONT size:6.0];
        self.lbl_opened_minutes.textColor = [UIColor blackColor];
        self.lbl_opened_minutes.backgroundColor = [UIColor clearColor];

        
        // title label
        self.lbl_menza_title = [[UILabel alloc] init];
        self.lbl_menza_title.frame = CGRectMake(5, 2, 75, 45); // 18
        self.lbl_menza_title.numberOfLines = 3;
        self.lbl_menza_title.textAlignment = NSTextAlignmentCenter;
        self.lbl_menza_title.font = [UIFont fontWithName:DEF_FONT size:10.0];
        self.lbl_menza_title.textColor = [UIColor whiteColor];
        self.lbl_menza_title.backgroundColor = [UIColor clearColor];
        
        self.img_bckg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 85, 45)];
        self.img_bckg.backgroundColor = [UIColor grayColor];
        self.img_bckg.alpha = 0.6;
        
        [self addSubview:self.img_bckg];
        [self addSubview:self.lbl_menza_title];
        [self addSubview:self.img_status];
        [self addSubview:self.img_opened];
        [self addSubview:self.lbl_opened_minutes];


        
    }
    return self;
}

-(void)setParentVC:(id)parentVC {
    
    // set title of menza
    self.lbl_menza_title.text = self.menza.title;
    
}




- (void)startJiggling {
    NSInteger randomInt = arc4random_uniform(500);
    float r = (randomInt/500.0)+0.5;
    
    CGAffineTransform leftWobble = CGAffineTransformMakeRotation(degreesToRadians( (kAnimationRotateDeg * -1.0) - r ));
    CGAffineTransform rightWobble = CGAffineTransformMakeRotation(degreesToRadians( kAnimationRotateDeg + r ));
    
    self.transform = leftWobble;  // starting point
    
    [[self layer] setAnchorPoint:CGPointMake(0.5, 0.5)];
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         [UIView setAnimationRepeatCount:NSNotFound];
                         self.transform = rightWobble; }
                     completion:nil];
}

-(void)noJiggling {
        [self.layer removeAllAnimations];
        self.transform = CGAffineTransformIdentity;
    }


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
