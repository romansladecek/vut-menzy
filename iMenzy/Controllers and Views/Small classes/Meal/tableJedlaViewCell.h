//
//  tableJedlaViewCell.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Table Cell for meals
//

#import <UIKit/UIKit.h>

@interface tableJedlaViewCell : UITableViewCell

@property (nonatomic, retain) UIImageView *img_bckg;

@property (nonatomic, retain) UILabel *lbl_name;
@property (nonatomic, retain) UILabel *lbl_cena1;


@end
