//
//  Meal.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Meal object
//

#import <Foundation/Foundation.h>

@interface Meal : NSObject {
    
    int idd;
	NSString *price1;
	NSString *price2;
    NSString *price3;
	NSString *gram;
	NSString *title;
	NSString *category;
    int order;
}

@property (assign, readwrite) int idd;
@property (nonatomic, retain) NSString *price1;
@property (nonatomic, retain) NSString *price2;
@property (nonatomic, retain) NSString *price3;
@property (nonatomic, retain) NSString *gram;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *category;
@property (assign, readwrite) int order;

-(id)initWithJedlo:(NSMutableDictionary *)meal;

@end
