//
//  MealCategoriesScrollView.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Meal Category scroll view
//

#import <UIKit/UIKit.h>

@interface MealCategoriesScrollView : UIScrollView

@property (nonatomic, retain) id parentVC;

- (id)initWithFrame:(CGRect)frame andParent:(id)parent;


@end
