//
//  MealCategoryButton.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Meal Category button
//

#import "MealCategoryButton.h"

@implementation MealCategoryButton

- (id)initWithCategory:(NSString *)category
{
    self = [super init];
    if (self) {
        
        self.category = category;
        self.alpha = 0.6;
        
        self.category_title = [[UILabel alloc] init];
        
        self.category_title.frame = CGRectMake(0, 0, CATEGORY_BUTTON_WIDTH, 50);
        self.category_title.numberOfLines = 1;
        self.category_title.textAlignment = NSTextAlignmentCenter;
        self.category_title.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.category_title.textColor = [UIColor blackColor];
        [self addSubview:self.category_title];
        
        self.img_strip = [[UIImageView alloc] init];
        self.img_strip.backgroundColor = [UIColor greenColor];
        [self addSubview:self.img_strip];

        
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
