//
//  Meal.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Meal object
//

#import "Meal.h"

@implementation Meal

@synthesize idd, title, price1, price2, price3, gram, category, order;

-(id)initWithJedlo:(NSMutableDictionary *)meal {
    
	self.idd = [meal[@"id"] intValue];
	self.title = meal[@"jidlo"];
	self.price1 = meal[@"cena1"];
    self.price2 = meal[@"cena2"];
	self.price3 = meal[@"cena3"];
	self.gram = meal[@"gram"];
	self.category = meal[@"kategorie"];
    self.order = [meal[@"radek"] intValue];
	return self;
}




@end
