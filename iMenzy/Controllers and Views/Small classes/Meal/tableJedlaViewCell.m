//
//  tableJedlaViewCell.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Table Cell for meals
//

#import "tableJedlaViewCell.h"

@implementation tableJedlaViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setBackgroundColor:[UIColor clearColor]];
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        self.img_bckg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 30)];
        self.img_bckg.alpha = 0.6;
        [self addSubview:self.img_bckg];
        
        // name of meal
        self.lbl_name = [[UILabel alloc] initWithFrame:CGRectMake(10, 4, 330, 20)];
        [self.lbl_name setBackgroundColor:[UIColor clearColor]];
        [self.lbl_name setTextColor:[UIColor whiteColor]];
        [self.lbl_name setFont:[UIFont fontWithName:DEF_FONT size:11.0]];
        [self addSubview:self.lbl_name];
        
        // price
        self.lbl_cena1 = [[UILabel alloc] initWithFrame:CGRectMake(280, 4, 40, 20)];
        [self.lbl_cena1 setBackgroundColor:[UIColor clearColor]];
        [self.lbl_cena1 setFont:[UIFont fontWithName:DEF_FONT size:11.0]];
        [self.lbl_cena1 setTextColor:[UIColor whiteColor]];
        [self addSubview:self.lbl_cena1];


    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
