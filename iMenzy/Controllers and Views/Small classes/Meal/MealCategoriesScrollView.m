//
//  MealCategoriesScrollView.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Meal Category scroll view
//

#import "MealCategoriesScrollView.h"
#import "ZoznamMenzViewController.h"
#import "NSArray+MatArray.h"
#import "MealCategoryButton.h"


@implementation MealCategoriesScrollView

- (id)initWithFrame:(CGRect)frame andParent:(id)parent
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.parentVC = parent;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    
    NSString *id_selected_menza = [self.parentVC id_selected_menzy];
    int numberOfSections = [global_jidlaDict[id_selected_menza] count];
    ;
    
    NSString *return_title;

    
    // Fill in menza's scrollview
    for (int i = 0; i < numberOfSections; i++) {
        
        
        // all keys
        NSArray*keys=[global_jidlaDict[id_selected_menza] allKeys];
        // sorting
        NSArray *sorted = [keys sortedArrayUsingArray:global_needsSorting];
        

        return_title = [sorted objectAtIndex:i];

        MealCategoryButton *btn_category = [[MealCategoryButton alloc] initWithCategory:return_title];

        btn_category.frame = CGRectMake(i*CATEGORY_BUTTON_WIDTH, 0, CATEGORY_BUTTON_WIDTH, 50);
        [[btn_category category_title] setText:return_title];
        [btn_category setBackgroundColor:UIColorFromRGB(0xffffff)];
        [btn_category addTarget:self.parentVC
                     action:@selector(mealCategoryPressed:)
           forControlEvents:UIControlEventTouchUpInside];
        [btn_category.img_strip setFrame:CGRectMake(0, btn_category.frame.size.height-3, CATEGORY_BUTTON_WIDTH, 3)];
        
        // selected menza highlighted
        if ([[self.parentVC id_selected_category] isEqualToString:btn_category.category_title.text]) {
            btn_category.img_strip.hidden = NO;
        }
        else {
            btn_category.img_strip.hidden = YES;
        }

        
        
        [self addSubview:btn_category];
        
    }
     
    self.contentSize = CGSizeMake(numberOfSections*CATEGORY_BUTTON_WIDTH, 50);
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
