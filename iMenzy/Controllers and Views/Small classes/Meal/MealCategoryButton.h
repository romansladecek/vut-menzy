//
//  MealCategoryButton.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Class for Meal Category button
//

#import <UIKit/UIKit.h>

@interface MealCategoryButton : UIButton

@property (nonatomic, retain) NSString *category;

@property (nonatomic, retain) UILabel *category_title;
@property (nonatomic, retain) UIImageView *img_strip;


- (id)initWithCategory:(NSString *)category;

@end
