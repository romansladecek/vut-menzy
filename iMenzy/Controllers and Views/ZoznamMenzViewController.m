//
//  ZoznamMenzViewController.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Main controller for app - table of meals and menzy
//


#import "ZoznamMenzViewController.h"
#import "MainController.h"
#import "Menza.h"
#import "tableJedlaViewCell.h"
#import "Meal.h"
#import "NSArray+MatArray.h"
#import "MealCategoryButton.h"
#import "MenzyCollectionViewCell.h"
//#import "LocationManager.h"
#import "MTPopupWindow.h"


#define M_PI   3.14159265358979323846264338327950288   /* pi */
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)


@interface ZoznamMenzViewController ()

@end

@implementation ZoznamMenzViewController

- (id)init
{
    self = [super init];
    if (self) {

        // load all XMLs and fill all global variables
        [self loadAllRemoteSources];
        
        UIImage *img;
        if (!isLongScreen) { // iph4
            img = [UIImage imageNamed:@"background_iph4"];
        }
        else {
            img = [UIImage imageNamed:@"background-568h"];
        }
        UIImageView *img_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, img.size.width, img.size.height)];
        img_background.image = img;
        img_background.alpha = 0.4;
        [self.view addSubview:img_background];
        
        [self.view setBackgroundColor:UIColorFromRGB(0x444444)];
        
        // default meal category
        self.id_selected_category = @"Hl. jídlo";

        self.editModeON = NO;
        //self.mapModeON = NO;
        self.clockModeON = NO;

        // title of app
        self.lbl_main_KaM_title = [[UILabel alloc] init];
        self.lbl_main_KaM_title.frame = CGRectMake(20, 8+global_ios7top, 150, 30);
        self.lbl_main_KaM_title.numberOfLines = 1;
        self.lbl_main_KaM_title.textAlignment = NSTextAlignmentCenter;
        self.lbl_main_KaM_title.font = [UIFont fontWithName:DEF_FONT size:14.0];
        self.lbl_main_KaM_title.textColor = [UIColor whiteColor];
        self.lbl_main_KaM_title.backgroundColor = [UIColor clearColor];
        self.lbl_main_KaM_title.text = @"VUT JÍDELNÍČEK";
        self.lbl_main_KaM_title.alpha = 1.0;
        [self.view addSubview:self.lbl_main_KaM_title];
        
        
        // settings mode button
        self.btn_settings = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn_settings.frame = CGRectMake(265, 8+global_ios7top, 30, 30);
        [self.btn_settings addTarget:self
                            action:@selector(btn_settingsModePressed)
                  forControlEvents:UIControlEventTouchUpInside];
        [self.btn_settings setBackgroundImage:[UIImage imageNamed:@"settings_icon.png"] forState:UIControlStateNormal];
        [self.view addSubview:self.btn_settings];
        
        
        // opening hours buttons view (to be together)
        self.view_opening_hours = [[UIView alloc] initWithFrame:CGRectMake(225, 8+global_ios7top, 30, 30)];
        [self.view addSubview:self.view_opening_hours];
        
        // opening hours mode button
        self.btn_opening_hours = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn_opening_hours.frame = CGRectMake(0, 0, 30, 30);
        [self.btn_opening_hours addTarget:self
                              action:@selector(btn_clockModePressed)
                    forControlEvents:UIControlEventTouchUpInside];
        [self.btn_opening_hours setBackgroundImage:[UIImage imageNamed:@"clock_icon.png"] forState:UIControlStateNormal];
        [self.view_opening_hours addSubview:self.btn_opening_hours];
        
        // + spinner to clock mode button
        self.btn_opening_hours_hand = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn_opening_hours_hand.frame = CGRectMake(0, 0, 30, 30);
        [self.btn_opening_hours_hand addTarget:self
                                 action:@selector(btn_clockModePressed)
                       forControlEvents:UIControlEventTouchUpInside];
        [self.btn_opening_hours_hand setBackgroundImage:[UIImage imageNamed:@"clock_icon_hand.png"] forState:UIControlStateNormal];
        [self.view_opening_hours addSubview:self.btn_opening_hours_hand];
 
        
        /*
        // map buttons view (to be together)
        self.view_map = [[UIView alloc] initWithFrame:CGRectMake(185, 8+global_ios7top, 30, 30)];
        //[self.view addSubview:self.view_map];
        
        // map mode button
        self.btn_map = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn_map.frame = CGRectMake(0, 0, 30, 30);
        [self.btn_map addTarget:self
                                   action:@selector(btn_mapModePressed)
                         forControlEvents:UIControlEventTouchUpInside];
        [self.btn_map setBackgroundImage:[UIImage imageNamed:@"map_icon.png"] forState:UIControlStateNormal];
        [self.view_map addSubview:self.btn_map];
        
        // + spinner to map mode button
        self.btn_map_spinner = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn_map_spinner.frame = CGRectMake(10,0, 15, 15);
        [self.btn_map_spinner addTarget:self
                         action:@selector(btn_mapModePressed)
               forControlEvents:UIControlEventTouchUpInside];
        [self.btn_map_spinner setBackgroundImage:[UIImage imageNamed:@"map_icon_spinner.png"] forState:UIControlStateNormal];
        [self.view_map addSubview:self.btn_map_spinner];
        */
        
        self.lbl_title = [[UILabel alloc] init];
        self.lbl_title.numberOfLines = 1;
        self.lbl_title.textAlignment = NSTextAlignmentCenter;
        self.lbl_title.font = [UIFont fontWithName:DEF_FONT size:14.0];
        self.lbl_title.textColor = [UIColor whiteColor];
        self.lbl_title.backgroundColor = [UIColor clearColor];
        self.lbl_title.alpha = 0.0;
        [self.view addSubview:self.lbl_title];

        
        
        // refresh circle view
        self.percentage_progressCircleView = 0;
        CGFloat radius = 25 * 0.8;
        self.progressCircleView = [[THCircularProgressView alloc] initWithCenter:CGPointMake(295, 117+global_ios7top)
                                                                                         radius:radius
                                                                                      lineWidth:1.0f
                                                                                   progressMode:THProgressModeDeplete
                                                                                  progressColor:[UIColor whiteColor]
                                                                         progressBackgroundMode:THProgressBackgroundModeCircumference
                                                                        progressBackgroundColor:[UIColor colorWithRed:0.96f green:0.96f blue:0.96f alpha:0.2f]
                                                                                     percentage:self.percentage_progressCircleView];
        [self.view addSubview:self.progressCircleView];
        
        // create timer
        self.timer_progressCircleView = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                      target:self
                                                    selector:@selector(timer_progressCircleView_Fired:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        // timer on background
        [[NSRunLoop mainRunLoop] addTimer:self.timer_progressCircleView forMode:NSRunLoopCommonModes];
        
        // refresh button on circle
        self.btn_refreshAll = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn_refreshAll.frame = CGRectMake(273, 95+global_ios7top, 44, 44);
        [self.btn_refreshAll setImage:[UIImage imageNamed:@"refreshButton.png"] forState:UIControlStateNormal];
        self.btn_refreshAll.titleLabel.textColor = [UIColor whiteColor];
        self.btn_refreshAll.titleLabel.textAlignment = NSTextAlignmentRight;
        self.btn_refreshAll.backgroundColor = [UIColor clearColor];
        [self.btn_refreshAll addTarget:self
                         action:@selector(refreshAll)
               forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.btn_refreshAll];
        

        
        
        
        
        
        // menzy
        self.layout=[[LXReorderableCollectionViewFlowLayout alloc] init];
        [self.layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        //[self.layout setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)]; // entire collection view
        //layout.itemSize = ...;
        //layout.minimumInteritemSpacing = 0.0;
        self.layout.minimumLineSpacing = 1.0; // spaces between items
        
        self.menzy_collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 44+global_ios7top, self.view.frame.size.width, 49) collectionViewLayout:self.layout];
        [self.menzy_collectionView setDataSource:self];
        [self.menzy_collectionView setDelegate:self];
        [self.menzy_collectionView registerClass:[MenzyCollectionViewCell class] forCellWithReuseIdentifier:@"menzyCellIdentifier"];
        [self.menzy_collectionView setBackgroundColor:[UIColor clearColor]];


        

        
        // category of meals
        self.scroll_meal_categories = [[MealCategoriesScrollView alloc] initWithFrame:CGRectMake(0, 44+global_ios7top+self.menzy_collectionView.frame.size.height, 270, 50) andParent:self];
        self.scroll_meal_categories.backgroundColor = [UIColor clearColor];
        
        
        
        
        
        // table of meals
        self.table_jedla = [[UITableView alloc] initWithFrame:CGRectMake(0, 44+global_ios7top+self.menzy_collectionView.frame.size.height+self.scroll_meal_categories.frame.size.height+2, self.view.frame.size.width, self.view.frame.size.height-self.menzy_collectionView.frame.size.height-self.scroll_meal_categories.frame.size.height-44-global_ios7top) style:UITableViewStylePlain];
        [self.table_jedla setBackgroundColor:[UIColor clearColor]];
        [self.table_jedla setDelegate:self];
        [self.table_jedla setDataSource:self];
        if (IOS7_VERSION) {
            [self.table_jedla setSeparatorInset:UIEdgeInsetsZero];
        }
        [self.table_jedla setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        
        
        // opening hours
        self.openingHoursView = [[OpeningHoursView alloc] init];
        self.openingHoursView.frame = CGRectMake(30, 140+global_ios7top, 260, 150);
        self.openingHoursView.alpha = 0.0;
        
        // no meals view
        self.noMealsView = [[NoMealsView alloc] init];
        self.noMealsView.frame = CGRectMake(30, 140+global_ios7top, 260, 150);
        self.noMealsView.alpha = 0.0;
        
        // settings view
        self.settingsView = [[SettingsView alloc] initWithParent:self];
        self.settingsView.frame = CGRectMake(30, 140+global_ios7top, 260, 150);
        self.settingsView.alpha = 0.0;
 
        // map view
        //self.mapAndDirectionView = [[MapAndDirectionView alloc] initWithParent:self];
        //self.mapAndDirectionView.frame = CGRectMake(30, 140+global_ios7top, 260, 150);
        //self.mapAndDirectionView.alpha = 0.0;
        
        [self.view addSubview:self.openingHoursView];
        [self.view addSubview:self.noMealsView];
        [self.view addSubview:self.settingsView];
        //[self.view addSubview:self.mapAndDirectionView];

        [self.view addSubview:self.menzy_collectionView];
        [self.view addSubview:self.table_jedla];
        [self.view addSubview:self.scroll_meal_categories];
        
        
        // location manager bcs of mylocation
        //self.locationManager = [[CLLocationManager alloc] init];
        //self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
        //self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation; // 100 m
        
        
        NSIndexPath *ip = [NSIndexPath indexPathForRow:0 inSection:0] ;
        [self collectionView:self.menzy_collectionView didSelectItemAtIndexPath:ip];


        
    }
    return self;
}

/*
-(CLLocation *)deviceLocation {
    // my location
    
    //CLLocation *location = [LocationManager currentLocationByWaitingUpToMilliseconds:100];

    return [[CLLocation alloc] initWithLatitude:self.locationManager.location.coordinate.latitude longitude:self.locationManager.location.coordinate.longitude];
    
    
}
 */

// refresh circle view - timer exceeted time
- (void)timer_progressCircleView_Fired:(NSTimer *)timer
{
    self.percentage_progressCircleView += 0.0002;
    if (self.percentage_progressCircleView >= 1) {
        [self refreshAll];
    }
    
    self.progressCircleView.percentage = self.percentage_progressCircleView;
    
}




// refresh button pressed / time for resfresh reached
-(void)refreshAll {
    
    self.btn_refreshAll.hidden = YES;
    self.percentage_progressCircleView = 0;
    
    // on background
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        // load new jidla xml
        [MainController loadJidlaResource];

        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [self.menzy_collectionView reloadData];
            
            // reload jidla
            //[self.table_jedla reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
            //[self.table_jedla setContentOffset:CGPointMake(0, 0) animated:NO];
            [self.table_jedla reloadData];
            
            self.btn_refreshAll.hidden = NO;

       });
        

     });

}

// download / load all XMLs
-(void)loadAllRemoteSources {
    
    [MainController loadMenzyResource];
    [MainController loadOpeningHoursResource];
    [MainController loadJidlaResource];
}

/*
-(void)btn_mapModePressed {
    
    if (self.mapModeON) {
        self.mapModeON = NO;
    
        [self.locationManager stopUpdatingLocation];
        [self.mapAndDirectionView.locationManager stopUpdatingLocation];
        [self.mapAndDirectionView.locationManager stopUpdatingHeading];
        
        [self.btn_map_spinner.layer removeAllAnimations];// removeAnimationForKey:@"SpinAnimation2"];
        
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             
                             [self.table_jedla setFrame:CGRectMake(0, self.table_jedla.frame.origin.y-200, self.table_jedla.frame.size.width, self.table_jedla.frame.size.height)];
                             
                             
                             [self.scroll_meal_categories setFrame:CGRectMake(0, self.scroll_meal_categories.frame.origin.y, self.scroll_meal_categories.frame.size.width, self.scroll_meal_categories.frame.size.height)];
                             
                             [self.table_jedla setUserInteractionEnabled:YES];
                             [self.table_jedla setAlpha:1.0];
                             [self.scroll_meal_categories setUserInteractionEnabled:YES];
                             [self.scroll_meal_categories setAlpha:1.0];
                             
                             self.lbl_title.frame = CGRectMake(130, 8+global_ios7top, 230, 30);
                             self.lbl_title.alpha = 0.0;
                             
                             self.lbl_main_KaM_title.frame = CGRectMake(20, 8+global_ios7top, 150, 30);
                             self.lbl_main_KaM_title.alpha = 1.0;
                             
                             self.btn_settings.frame = CGRectMake(265, 8+global_ios7top, 30, 30);
                             self.btn_settings.alpha = 1.0;
                             
                             self.view_map.frame = CGRectMake(185, 8+global_ios7top, 30, 30);
                             
                             self.noMealsView.alpha = self.backup_alpha_noMealsView;
                             
                             self.mapAndDirectionView.alpha = 0.0;
 
                            self.progressCircleView.center = CGPointMake(295, 117+global_ios7top);
                            self.btn_refreshAll.frame = CGRectMake(272, 93+global_ios7top, 47, 50);
 
                         } completion:NULL];
        
        [UIView animateWithDuration:0.5
                              delay:0.2
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             self.view_opening_hours.frame = CGRectMake(225, 8+global_ios7top, 30, 30);
                             self.view_opening_hours.alpha = 1.0;
                             
                         }  completion:NULL];

        
        
    }
    else {
        self.mapModeON = YES;
        
        [self.locationManager startUpdatingLocation];
        [self.mapAndDirectionView.locationManager startUpdatingLocation];
        [self.mapAndDirectionView.locationManager startUpdatingHeading];

        
        int numberOfSections = [global_jidlaDict[self.id_selected_menzy] count];
        if (numberOfSections > 3) numberOfSections = 3; // fix if more than 3 sections
        
        // rotation by axis y
        [UIView beginAnimations:nil context:nil];
        CATransform3D _3Dt = CATransform3DRotate(self.btn_map_spinner.layer.transform,3.14, 0.0, 1.0,0.0);
        [UIView setAnimationRepeatCount:10000];
        [UIView setAnimationDuration:1.0];
        self.btn_map_spinner.layer.transform=_3Dt;
        [UIView commitAnimations];
        
        

        
        self.lbl_title.text = @"Kudy do menzy?";
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             
                             [self.table_jedla setFrame:CGRectMake(0, self.table_jedla.frame.origin.y+200, self.table_jedla.frame.size.width, self.table_jedla.frame.size.height)];
                             [self.scroll_meal_categories setFrame:CGRectMake(-270, self.scroll_meal_categories.frame.origin.y, self.scroll_meal_categories.frame.size.width, self.scroll_meal_categories.frame.size.height)];
                             
                             
                             
                             [self.table_jedla setUserInteractionEnabled:NO];
                             [self.table_jedla setAlpha:0.5];
                             
                             [self.scroll_meal_categories setUserInteractionEnabled:NO];
                             [self.scroll_meal_categories setAlpha:0.5];
                             
                             
                             
                             self.lbl_title.frame = CGRectMake(20, 8+global_ios7top, 230, 30);
                             self.lbl_title.alpha = 1.0;
                             
                             self.lbl_main_KaM_title.frame = CGRectMake(-120, 8+global_ios7top, 150, 30);
                             self.lbl_main_KaM_title.alpha = 0.0;
                             
                             self.view_opening_hours.frame = CGRectMake(225, -130, 30, 30);
                             self.view_opening_hours.alpha = 1.0;
                             
                             self.backup_alpha_noMealsView = self.noMealsView.alpha;
                             self.noMealsView.alpha = 0.0;
                             
                             self.mapAndDirectionView.alpha = 1.0;
 
                            self.progressCircleView.center = CGPointMake(345, 117+global_ios7top);
                            self.btn_refreshAll.frame = CGRectMake(322, 93+global_ios7top, 47, 50);


 
                         } completion:NULL];
        
        [UIView animateWithDuration:0.5
                              delay:0.2
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             
                             self.btn_settings.frame = CGRectMake(265, -130, 30, 30);
                             self.btn_settings.alpha = 0.0;
                             
                             self.view_map.frame = CGRectMake(265, 8+global_ios7top, 30, 30);
                             
                         }  completion:NULL];
        

        
    }

    
}
 */

// Opening hours mode
-(void)btn_clockModePressed {
    


    
    if (self.clockModeON) {
        self.clockModeON = NO;
        
        [self.btn_opening_hours.layer removeAnimationForKey:@"SpinAnimation3"];
        [self.btn_opening_hours_hand.layer removeAnimationForKey:@"SpinAnimation4"];

        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             
                             [self.table_jedla setFrame:CGRectMake(0, self.table_jedla.frame.origin.y-200, self.table_jedla.frame.size.width, self.table_jedla.frame.size.height)];
                             
                             
                             [self.scroll_meal_categories setFrame:CGRectMake(0, self.scroll_meal_categories.frame.origin.y, self.scroll_meal_categories.frame.size.width, self.scroll_meal_categories.frame.size.height)];
                             
                             [self.table_jedla setUserInteractionEnabled:YES];
                             [self.table_jedla setAlpha:1.0];
                             [self.scroll_meal_categories setUserInteractionEnabled:YES];
                             [self.scroll_meal_categories setAlpha:1.0];
                             
                             self.lbl_title.frame = CGRectMake(130, 8+global_ios7top, 230, 30);
                             self.lbl_title.alpha = 0.0;
                             
                             self.lbl_main_KaM_title.frame = CGRectMake(20, 8+global_ios7top, 150, 30);
                             self.lbl_main_KaM_title.alpha = 1.0;
                             
                             self.btn_settings.frame = CGRectMake(265, 8+global_ios7top, 30, 30);
                             self.btn_settings.alpha = 1.0;
                             
                             //self.view_map.frame = CGRectMake(185, 8+global_ios7top, 30, 30);
                             //self.view_map.alpha = 1.0;

                             self.view_opening_hours.frame = CGRectMake(225, 8+global_ios7top, 30, 30);
                             self.view_opening_hours.alpha = 1.0;
                             
                             self.openingHoursView.alpha = 0.0;
                             
                             self.noMealsView.alpha = self.backup_alpha_noMealsView;
                             
                             self.progressCircleView.center = CGPointMake(295, 117+global_ios7top);
                             self.btn_refreshAll.frame = CGRectMake(273, 95+global_ios7top, 44, 44);

                             
                         } completion:NULL];
        
        
        
        
    }
    else {
        self.clockModeON = YES;
        
        int numberOfSections = [global_jidlaDict[self.id_selected_menzy] count];
        if (numberOfSections > 3) numberOfSections = 3; // fix if more than 3 sections
        
        
        if ([self.btn_opening_hours.layer animationForKey:@"SpinAnimation3"] == nil) {
            CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
            animation.fromValue = [NSNumber numberWithFloat:0.0f];
            animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
            animation.duration = 24.0f;
            animation.repeatCount = INFINITY;
            [self.btn_opening_hours.layer addAnimation:animation forKey:@"SpinAnimation3"];
        }
        
        if ([self.btn_opening_hours_hand.layer animationForKey:@"SpinAnimation4"] == nil) {
            CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
            animation.fromValue = [NSNumber numberWithFloat:0.0f];
            animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
            animation.duration = 2.0f;
            animation.repeatCount = INFINITY;
            [self.btn_opening_hours_hand.layer addAnimation:animation forKey:@"SpinAnimation4"];
        }
        
        
        self.lbl_title.text = @"Otevírací hodiny";
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             
                             
                             [self.table_jedla setFrame:CGRectMake(0, self.table_jedla.frame.origin.y+200, self.table_jedla.frame.size.width, self.table_jedla.frame.size.height)];
                             [self.scroll_meal_categories setFrame:CGRectMake(-270, self.scroll_meal_categories.frame.origin.y, self.scroll_meal_categories.frame.size.width, self.scroll_meal_categories.frame.size.height)];
                             
                             
                             
                             [self.table_jedla setUserInteractionEnabled:NO];
                             [self.table_jedla setAlpha:0.5];
                             
                             [self.scroll_meal_categories setUserInteractionEnabled:NO];
                             [self.scroll_meal_categories setAlpha:0.5];
                             
                             
                             
                             self.lbl_title.frame = CGRectMake(20, 8+global_ios7top, 230, 30);
                             self.lbl_title.alpha = 1.0;
                             
                             self.lbl_main_KaM_title.frame = CGRectMake(-120, 8+global_ios7top, 150, 30);
                             self.lbl_main_KaM_title.alpha = 0.0;
                             
                             //self.view_map.frame = CGRectMake(185, -130, 30, 30);
                             //self.view_map.alpha = 0.0;
                             
                             self.btn_settings.frame = CGRectMake(330, 8+global_ios7top, 30, 30);
                             self.btn_settings.alpha = 0.0;
                             
                             self.view_opening_hours.frame = CGRectMake(265, 8+global_ios7top, 30, 30);
                             
                             self.openingHoursView.alpha = 1.0;

                             self.backup_alpha_noMealsView = self.noMealsView.alpha;
                             self.noMealsView.alpha = 0.0;
                             
                             self.progressCircleView.center = CGPointMake(345, 117+global_ios7top);
                             self.btn_refreshAll.frame = CGRectMake(322, 95+global_ios7top, 44, 44);

                         } completion:NULL];
        

        
        
        
    }
    
    
}




// edit button
-(void)btn_settingsModePressed {

    

    

        if (self.editModeON) {
            self.editModeON = NO;

            
            [self.btn_settings.layer removeAnimationForKey:@"SpinAnimation"];


            
            
            
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionAllowUserInteraction
                             animations:^{

                                 
                                 [self.table_jedla setFrame:CGRectMake(0, self.table_jedla.frame.origin.y-200, self.table_jedla.frame.size.width, self.table_jedla.frame.size.height)];
                                 
                                 
                                 [self.scroll_meal_categories setFrame:CGRectMake(0, self.scroll_meal_categories.frame.origin.y, self.scroll_meal_categories.frame.size.width, self.scroll_meal_categories.frame.size.height)];
                                 
                                 [self.table_jedla setUserInteractionEnabled:YES];
                                 [self.table_jedla setAlpha:1.0];
                                 [self.scroll_meal_categories setUserInteractionEnabled:YES];
                                 [self.scroll_meal_categories setAlpha:1.0];
                                 
                                 self.lbl_title.frame = CGRectMake(130, 8+global_ios7top, 230, 30);
                                 self.lbl_title.alpha = 0.0;
                                 
                                 self.lbl_main_KaM_title.frame = CGRectMake(20, 8+global_ios7top, 150, 30);
                                 self.lbl_main_KaM_title.alpha = 1.0;
                                 
                                 self.view_opening_hours.frame = CGRectMake(225, 8+global_ios7top, 30, 30);
                                 self.view_opening_hours.alpha = 1.0;

                                 self.noMealsView.alpha = self.backup_alpha_noMealsView;
                                 
                                 self.settingsView.alpha = 0.0;
                                 
                                 self.progressCircleView.center = CGPointMake(295, 117+global_ios7top);
                                 self.btn_refreshAll.frame = CGRectMake(273, 95+global_ios7top, 44, 44);

                                 
                             } completion:NULL];
            
            /*
            [UIView animateWithDuration:0.5
                                  delay:0.2
                                options:UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 
                                 self.view_map.frame = CGRectMake(185, 8+global_ios7top, 30, 30);
                                 self.view_map.alpha = 1.0;
                                 
                             }  completion:NULL];
             */

            
            

        }
        else {
            self.editModeON = YES;
            
            int numberOfSections = [global_jidlaDict[self.id_selected_menzy] count];
            if (numberOfSections > 3) numberOfSections = 3; // fix if more than 3 sections
            
            
            //[self.btn_settings setTitle:@"Done" forState:UIControlStateNormal];
            //self.layout.minimumLineSpacing = 1.0; // spaces between items
            //[self.menzy_collectionView setCollectionViewLayout:self.layout];
            
            if ([self.btn_settings.layer animationForKey:@"SpinAnimation"] == nil) {
                CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
                animation.fromValue = [NSNumber numberWithFloat:0.0f];
                animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
                animation.duration = 3.0f;
                animation.repeatCount = INFINITY;
                [self.btn_settings.layer addAnimation:animation forKey:@"SpinAnimation"];
            }
            

            
            

            
            self.lbl_title.text = @"Nastavení";
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 
                                 
                                 [self.table_jedla setFrame:CGRectMake(0, self.table_jedla.frame.origin.y+200, self.table_jedla.frame.size.width, self.table_jedla.frame.size.height)];
                                 [self.scroll_meal_categories setFrame:CGRectMake(-270, self.scroll_meal_categories.frame.origin.y, self.scroll_meal_categories.frame.size.width, self.scroll_meal_categories.frame.size.height)];
                                 
                                 
                                 
                                 [self.table_jedla setUserInteractionEnabled:NO];
                                 [self.table_jedla setAlpha:0.5];
                                 
                                 [self.scroll_meal_categories setUserInteractionEnabled:NO];
                                 [self.scroll_meal_categories setAlpha:0.5];

                                 
                                 
                                 self.lbl_title.frame = CGRectMake(20, 8+global_ios7top, 230, 30);
                                 self.lbl_title.alpha = 1.0;
                                 
                                 self.lbl_main_KaM_title.frame = CGRectMake(-120, 8+global_ios7top, 150, 30);
                                 self.lbl_main_KaM_title.alpha = 0.0;
                                 
                                 //self.view_map.frame = CGRectMake(185, -130, 30, 30);
                                 //self.view_map.alpha = 0.0;
                                 
                                 self.backup_alpha_noMealsView = self.noMealsView.alpha;
                                 self.noMealsView.alpha = 0.0;
                                 
                                 self.settingsView.alpha = 1.0;
                                 
                                 self.progressCircleView.center = CGPointMake(345, 117+global_ios7top);
                                 self.btn_refreshAll.frame = CGRectMake(322, 95+global_ios7top, 44, 44);


                                 
                             } completion:NULL];
            
            [UIView animateWithDuration:0.5
                                  delay:0.2
                                options:UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 

                                 
                                 self.view_opening_hours.frame = CGRectMake(225, -130, 30, 30);
                                 self.view_opening_hours.alpha = 0.0;
                                 
                             }  completion:NULL];


            
        }

    

    
    // show all visible and non visible menzy
    NSMutableDictionary *dictOfVisibleMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"visible_menzy"] mutableCopy];
    NSArray *arrayOfVisibleMenzy = [dictOfVisibleMenzy[@"YES"] mutableCopy];
    NSArray *arrayOfNotVisibleMenzy = [dictOfVisibleMenzy[@"NO"] mutableCopy];
    
    // order of menzy
    NSMutableArray *arrayOrderOfMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"order_menzy"] mutableCopy];
    
    // sort visible menzy by ordered menzy
    NSArray *sorted = [arrayOfVisibleMenzy sortedArrayUsingArray:arrayOrderOfMenzy];
    
    // save them
    NSMutableDictionary *outerDict = [[NSMutableDictionary alloc] init]; //== dictOfVisibleMenzy
    [outerDict setObject:sorted forKey:@"YES"];
    [outerDict setObject:arrayOfNotVisibleMenzy forKey:@"NO"];
    
    [[NSUserDefaults standardUserDefaults] setObject:outerDict forKey:@"visible_menzy"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // reload collection view
    [self.menzy_collectionView performBatchUpdates:^{
        [self.menzy_collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.menzy_collectionView.numberOfSections)]];
    } completion:^(BOOL finished) {}];
    


}




// Meal button pressed
-(void)mealCategoryPressed:(id)sender {
    
    MealCategoryButton *mealCategoryBtn = (MealCategoryButton *)sender;
    // set showed category
    self.id_selected_category = mealCategoryBtn.category;

    [self.scroll_meal_categories setNeedsDisplay];

    [self.table_jedla reloadData];
    [self.table_jedla setContentOffset:CGPointMake(0, 0) animated:NO];
    
    //NSLog(@"meal cat: %@", self.id_selected_category);

    
}



#pragma mark - Menzy - collectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    int number_of_menzy = 0;
    NSMutableDictionary *dictOfVisibleMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"visible_menzy"] mutableCopy];
    NSMutableArray *arrayOrderOfMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"order_menzy"] mutableCopy];


    if (self.editModeON) { // show all
        number_of_menzy = [arrayOrderOfMenzy count];
    }
    else { // show only visibile menzy
        number_of_menzy =  [dictOfVisibleMenzy[@"YES"] count];
    }
    
    return number_of_menzy;
}

/*
- (BOOL)respondsToSelector:(SEL)selector
{
    if (selector == @selector(collectionView:itemAtIndexPath:didMoveToIndexPath:))
    {
        if(self.editModeON)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return [super respondsToSelector:selector];
}
 */

// disable editing if there is not editing mode active
-(BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
   
    BOOL return_value = NO;
   
    if (self.editModeON) {
        return_value = YES;
    }
    
    return return_value;
}


// moving with menzy and changing order
- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath didMoveToIndexPath:(NSIndexPath *)toIndexPath {
    
    NSMutableArray *arrayOrderOfMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"order_menzy"] mutableCopy];

    id object = [arrayOrderOfMenzy objectAtIndex:fromIndexPath.item];
    [arrayOrderOfMenzy removeObjectAtIndex:fromIndexPath.item];
    [arrayOrderOfMenzy insertObject:object atIndex:toIndexPath.item];
    
    [[NSUserDefaults standardUserDefaults] setObject:arrayOrderOfMenzy forKey:@"order_menzy"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        
    [self.menzy_collectionView reloadData];

}

// cells in collection view
- (MenzyCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // new cell
    MenzyCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"menzyCellIdentifier" forIndexPath:indexPath];
    // visible menzy
    NSMutableDictionary *dictOfVisibleMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"visible_menzy"] mutableCopy];
    NSMutableArray *arrayOfVisibleMenzy = [[dictOfVisibleMenzy objectForKey:@"YES"] mutableCopy];
    // order of menzy
    NSMutableArray *arrayOrderOfMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"order_menzy"] mutableCopy];

    // EDITING MODE (all menzy)
    if (self.editModeON) {
    
        // get ID of menza
        NSMutableDictionary *oneMenzaIddInOrderedArray = [arrayOrderOfMenzy objectAtIndex:indexPath.row];
        // get Menza object from that ID
        NSMutableDictionary *oneMenzaDict = [global_menzyDict objectForKey:oneMenzaIddInOrderedArray];
        Menza *menza = [[Menza alloc] initWithMenza:oneMenzaDict];
        
        // pass Menza object to cell and set parent as this class
        cell.menza = menza;
        [cell setParentVC:self];
    
        // all cells are same color
        cell.img_bckg.backgroundColor = [UIColor lightGrayColor];

        // start jiggling
        [cell startJiggling];

        // show buttons add/delete - not visible/visible menzy // ToDo
        if ([arrayOfVisibleMenzy containsObject:menza.idd]) {
            //cell.btn_delete.alpha = 0.0;
            [cell.img_status setHidden:YES];

        }
        else {
            [cell.img_status setHidden:NO];
            //cell.btn_delete.alpha = 1.0;

        }
        cell.img_opened.hidden = YES;
        cell.lbl_opened_minutes.text = @"";
        




    }
    else { // NOT EDITING
    
        // get ID of menza (only from visible menzy array
        NSMutableDictionary *oneMenzaIddInOrderedArray = [arrayOfVisibleMenzy objectAtIndex:indexPath.row];
        // get Menza object from that ID
        NSMutableDictionary *oneMenzaDict = [global_menzyDict objectForKey:oneMenzaIddInOrderedArray];
        Menza *menza = [[Menza alloc] initWithMenza:oneMenzaDict];

        // pass Menza object to cell and set parent as this class
        cell.menza = menza;
        [cell setParentVC:self];
        
        // highlight selected menza
        if (self.id_selected_menzy == menza.idd) {
            cell.img_bckg.backgroundColor = UIColorFromRGB(0xcfcfcf);
        }
        else {
            cell.img_bckg.backgroundColor = [UIColor lightGrayColor];
        }
        
        // if menza is opened
        int minutesYet = [MainController isMenzaOpened:menza];
        if (minutesYet) {
            if (minutesYet < 30) {
                cell.img_opened.backgroundColor = UIColorFromRGB(0xfffc00);
                cell.lbl_opened_minutes.text = [NSString stringWithFormat:@"%d", minutesYet];
            }
            else {
                cell.img_opened.backgroundColor = UIColorFromRGB(0x00ff12);
                cell.lbl_opened_minutes.text = @"";
            }
            cell.img_opened.hidden = NO;
        }
        else {
            cell.img_opened.hidden = YES;
            cell.lbl_opened_minutes.text = @"";

        }

        // stop jiggling
        [cell noJiggling];
        // hide add/delete buttons
        [cell.img_status setHidden:YES];


    }
    
    return cell;
}

// selected some menza
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 
    NSMutableDictionary *dictOfVisibleMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"visible_menzy"] mutableCopy];
    NSMutableArray *arrayOfVisibleMenzy = [[dictOfVisibleMenzy objectForKey:@"YES"] mutableCopy];
    NSMutableArray *arrayOfNotVisibleMenzy = [[dictOfVisibleMenzy objectForKey:@"NO"] mutableCopy];
    
    
    if (!self.editModeON) {
        
        // get Menza from array of visible menzy
        NSString *oneMenzaIddInVisibleArray = [arrayOfVisibleMenzy objectAtIndex:indexPath.row];
        NSMutableDictionary *oneMenzaDict = [global_menzyDict objectForKey:oneMenzaIddInVisibleArray];
        Menza *menza = [[Menza alloc] initWithMenza:oneMenzaDict];
        
        // pass this as argument for id_selected_menzy
        self.id_selected_menzy = menza.idd;
        
        // reload categories of meal
        [self.scroll_meal_categories setNeedsDisplay];
        
        // reload meals
        [self.table_jedla reloadData];
        //[self.table_jedla reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        [self.table_jedla setContentOffset:CGPointMake(0, 0) animated:NO];
        
        
        //if (self.clockModeON) {
        

            
            // get times
            NSArray *days = [NSArray arrayWithObjects:@"1", @"5", @"6", @"7", nil];
            
            for (NSString *day in days) {
                
                if (global_otevrenoDict[menza.idd][day]) {
                    
                    NSString *dates = @"";
                    
                    int count = [global_otevrenoDict[menza.idd][day] count];
                    // all times (more times opened a day) - max 2 now
                    for (int i = 0; i < count; i++) {
                        
                        NSString *date = [NSString stringWithFormat:@"%@ - %@", [MainController dateToString:global_otevrenoDict[menza.idd][day][i][@"from"]], [MainController dateToString:global_otevrenoDict[menza.idd][day][i][@"to"]]];
                        
                        dates = [dates stringByAppendingString:date];
                        
                        if (i < count-1) {
                            dates = [dates stringByAppendingString:@"\n"];
                        }
                        
                    } // for
                    
                    if ([day isEqualToString:@"1"]) {
                        self.openingHoursView.lbl_monday_time.text = dates;
                        self.openingHoursView.lbl_monday.hidden = NO;
                        self.openingHoursView.lbl_monday_time.hidden = NO;
                    }
                    else if ([day isEqualToString:@"5"]) {
                        self.openingHoursView.lbl_friday_time.text = dates;
                        self.openingHoursView.lbl_friday.hidden = NO;
                        self.openingHoursView.lbl_friday_time.hidden = NO;
                    }
                    else if ([day isEqualToString:@"6"]) {
                        self.openingHoursView.lbl_saturday_time.text = dates;
                        self.openingHoursView.lbl_saturday.hidden = NO;
                        self.openingHoursView.lbl_saturday_time.hidden = NO;
                    }
                    else if ([day isEqualToString:@"7"]) {
                        self.openingHoursView.lbl_sunday_time.text = dates;
                        self.openingHoursView.lbl_sunday.hidden = NO;
                        self.openingHoursView.lbl_sunday_time.hidden = NO;
                    }

                } // if day exists
                else {
                    
                    // hide day
                    if ([day isEqualToString:@"1"]) {
                        self.openingHoursView.lbl_monday.hidden = YES;
                        self.openingHoursView.lbl_monday_time.hidden = YES;
                    }
                    else if ([day isEqualToString:@"5"]) {
                        self.openingHoursView.lbl_friday.hidden = YES;
                        self.openingHoursView.lbl_friday_time.hidden = YES;
                    }
                    else if ([day isEqualToString:@"6"]) {
                        self.openingHoursView.lbl_saturday.hidden = YES;
                        self.openingHoursView.lbl_saturday_time.hidden = YES;
                    }
                    else if ([day isEqualToString:@"7"]) {
                        self.openingHoursView.lbl_sunday.hidden = YES;
                        self.openingHoursView.lbl_sunday_time.hidden = YES;
                    }
                    
                }
                
            } // for days
            

       // } // if clockMode
        
        // distance in MapMode
        


        
        
    
    }
    else {
    
        NSMutableArray *arrayOrderOfMenzy = [[[NSUserDefaults standardUserDefaults] objectForKey:@"order_menzy"] mutableCopy];

        // get Menza from array of visible menzy
        NSString *oneMenzaIddInOrderedArray = [arrayOrderOfMenzy objectAtIndex:indexPath.row];
        NSMutableDictionary *oneMenzaDict = [global_menzyDict objectForKey:oneMenzaIddInOrderedArray];
        Menza *menza = [[Menza alloc] initWithMenza:oneMenzaDict];
        
        
        // switch them after clicked from visible to non, non to visible
        if ([arrayOfVisibleMenzy containsObject:menza.idd]) { // find current menza
            [arrayOfVisibleMenzy removeObject:menza.idd];
            [arrayOfNotVisibleMenzy addObject:menza.idd];
        }
        else { // was not visible
            [arrayOfNotVisibleMenzy removeObject:menza.idd];
            [arrayOfVisibleMenzy addObject:menza.idd];
        }
        
        // save them
        NSMutableDictionary *outerDict = [[NSMutableDictionary alloc] init]; //== dictOfVisibleMenzy
        [outerDict setObject:arrayOfVisibleMenzy forKey:@"YES"];
        [outerDict setObject:arrayOfNotVisibleMenzy forKey:@"NO"];
        
        [[NSUserDefaults standardUserDefaults] setObject:outerDict forKey:@"visible_menzy"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        

    }
    
    // reload menzy
    [self.menzy_collectionView performBatchUpdates:^{
        [self.menzy_collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:nil];
    
}

// rectangle for menzy
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(85, 49);
}




#pragma mark - Jidla - tableView
/*
 ** Height
 **
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}


/*
 ** Cells
 **
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *itemListTableViewCellVersoIdentifier = @"jedlaCellIdentifier";
    tableJedlaViewCell *cellMeal = [tableView dequeueReusableCellWithIdentifier:itemListTableViewCellVersoIdentifier];
    
    if (cellMeal == nil) {
		cellMeal = [[tableJedlaViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:itemListTableViewCellVersoIdentifier];
	}
    
    Meal *meal;

    // get meals for selected menza and selected category (if exists selected category)
    if ([global_jidlaDict[self.id_selected_menzy] objectForKey:self.id_selected_category]) {
         meal = [[Meal alloc] initWithJedlo:global_jidlaDict[self.id_selected_menzy][self.id_selected_category][indexPath.row]];
    }
    // if doesn't exist, select the first found category
    else {
        
        id val = nil;
        NSArray *values = [global_jidlaDict[self.id_selected_menzy] allValues];
        
        if ([values count] != 0) {
            val = [values objectAtIndex:0][0][@"kategorie"];
            meal = [[Meal alloc] initWithJedlo:global_jidlaDict[self.id_selected_menzy][val][indexPath.row]];
        }
        else {
            // no meals
        }

        self.id_selected_category = val;

    }

    // background of meals cells
    if (indexPath.row % 2) {
        cellMeal.img_bckg.backgroundColor = [UIColor lightGrayColor];
    }
    else {
        cellMeal.img_bckg.backgroundColor = [UIColor grayColor];
    }

    
    
    
    // set labels
    [cellMeal.lbl_name setText:meal.title];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"price_displayed"] intValue] == STUDENT) {
        [cellMeal.lbl_cena1 setText:[NSString stringWithFormat:@"%@ Kč", meal.price1]];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"price_displayed"] intValue] == EMPLOYEE) {
        [cellMeal.lbl_cena1 setText:[NSString stringWithFormat:@"%@ Kč", meal.price2]];
    }
    else {
        [cellMeal.lbl_cena1 setText:[NSString stringWithFormat:@"%@ Kč", meal.price3]];
    }
    
    // Google Image Search API - deprecated
    //NSString *url_imageSearch = [NSString stringWithFormat:@"https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=%@+recepty", meal.title];
    //NSMutableDictionary *imagesDict = [MainController doRequestToURL:url_imageSearch requestType:@"GET"];
    //NSLog(@"Images: %@", imagesDict);
    
    // Google Translator API
    //NSString *url_imageSearch = [NSString stringWithFormat:@"http://mymemory.translated.net/api/get?q=%@&langpair=cs|en", meal.title];
    //NSMutableDictionary *imagesDict = [MainController doRequestToURL:url_imageSearch requestType:@"GET"];
    //NSLog(@"Images: %@", imagesDict);
    

    /*
    NSString *urlPath = [NSString stringWithFormat:@"/translate_a/t?client=t&text=%@&langpair=en|fr",@"Hello"];
    
    NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:@"translate.google.com" path:urlPath];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse *response;
    NSError *error;
    NSData *data;
    data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    
    NSLog(@"Text: %@",result);
    */
    
    
    return cellMeal;
}

/*
 ** Number of rows
 **
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger return_value;
    
    // if category exists
    if (global_jidlaDict[self.id_selected_menzy][self.id_selected_category]) {
        return_value = [global_jidlaDict[self.id_selected_menzy][self.id_selected_category] count];
    }
    else {
        
        id val = nil;
        NSArray *values = [global_jidlaDict[self.id_selected_menzy] allValues];
        
        if ([values count] != 0) {
            val = [values objectAtIndex:0][0][@"kategorie"];
            return_value = [global_jidlaDict[self.id_selected_menzy][val] count];
        }
        else {
            return_value = 0;
        }
    }
    
    // is any meal
    if (return_value) {
        self.noMealsView.alpha = 0.0;
        self.backup_alpha_noMealsView = 0.0;
    }
    else {
        Menza *menza = [[Menza alloc] initWithMenza:global_menzyDict[self.id_selected_menzy]];
        // is opened but no meals
        if ([MainController isMenzaOpened:menza]) {
            self.noMealsView.lbl_status.text = @"Menza nezveřejnila jídelníček";
        }
        // not opened
        else {
            self.noMealsView.lbl_status.text = @"Menza je zavřena";
        }
        
        // save visibility of status label
        if (self.editModeON || self.clockModeON) {// || self.mapModeON) {
            self.backup_alpha_noMealsView = 1.0;
        }
        else {
            self.noMealsView.alpha = 1.0;
            self.backup_alpha_noMealsView = 1.0;

        }
    }

    return return_value;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


/*
 ** Selected cell -> consistency
 **
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableJedlaViewCell *cell = (tableJedlaViewCell *)[self.table_jedla cellForRowAtIndexPath:indexPath];

    // show images of meal (google search)
    NSString *url = [NSString stringWithFormat:@"http://www.google.com/search?tbm=isch&q=%@", cell.lbl_name.text];
    NSString *encodedString=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [MTPopupWindow showWindowWithHTMLFile:encodedString insideView:self.view];

    
}

// bcs of easter egg
-(void)viewDidAppear:(BOOL)animated {
    [self.settingsView becomeFirstResponder];    
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [self.settingsView resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
 
    

    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
