//
//  TutorialViewController.h
//  iMenzy
//
//  Created by Roman Sladecek on 11/14/13.
//  Copyright (c) 2013 FIT VUT Brno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController

@property (nonatomic, retain) UIScrollView *scroll;
@property (nonatomic, retain) UIButton *btn_go;

@end
