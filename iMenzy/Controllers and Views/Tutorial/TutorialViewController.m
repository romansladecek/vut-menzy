//
//  TutorialViewController.m
//  iMenzy
//
//  Created by Roman Sladecek on 11/14/13.
//  Copyright (c) 2013 FIT VUT Brno. All rights reserved.
//

#import "TutorialViewController.h"
#import "AppDelegate.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:UIColorFromRGB(0x444444)];
        UIImage *img;
        if (!isLongScreen) { // iph4
            img = [UIImage imageNamed:@"background_iph4"];
        }
        else {
            img = [UIImage imageNamed:@"background-568h"];
        }
        UIImageView *background = [[UIImageView alloc] init];
        background.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        background.image = img;
        background.alpha = 0.4;
        [self.view addSubview:background];
        
        self.scroll = [[UIScrollView alloc] init];
        self.scroll.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        self.scroll.contentSize = CGSizeMake(SCREEN_WIDTH*4, SCREEN_HEIGHT);
        self.scroll.pagingEnabled = YES;
        [self.view addSubview:self.scroll];
        
        UIImageView *tutorial = [[UIImageView alloc] init];
        tutorial.frame = CGRectMake(0, 0, SCREEN_WIDTH*4, SCREEN_HEIGHT);
        if (!isLongScreen) { // iph4
            tutorial.image = [UIImage imageNamed:@"tutorial"];
        }
        else {
            tutorial.image = [UIImage imageNamed:@"tutorial-568h"];
        }
        [self.scroll addSubview:tutorial];
        
        // button
        UIImageView *btn_bckg = [[UIImageView alloc] init];
        btn_bckg.frame = CGRectMake(SCREEN_WIDTH*3 + 35, 270, 250, 100);
        btn_bckg.backgroundColor = UIColorFromRGB(0xe2e2e2);
        btn_bckg.alpha = 0.2;
        [self.scroll addSubview:btn_bckg];
        
        self.btn_go = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn_go.frame = CGRectMake(SCREEN_WIDTH*3 + 35, 270, 250, 100);
        [self.btn_go addTarget:self
                                        action:@selector(btn_goPressed)
                              forControlEvents:UIControlEventTouchUpInside];
        [self.btn_go setTitle:@"Ukáž ponuku" forState:UIControlStateNormal];
        [self.btn_go setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.btn_go.titleLabel setFont:[UIFont fontWithName:DEF_FONT size:36]];
        [self.btn_go setBackgroundColor:[UIColor clearColor]];
        [self.scroll addSubview:self.btn_go];
        
        
    }
    return self;
}

-(void) btn_goPressed {
    
    // change root VC
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [UIView transitionWithView:appDelegate.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{ appDelegate.window.rootViewController = (UIViewController *)appDelegate.zoznamMenzViewController; }
                    completion:nil];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"show_tutorial"];
    
    
    NSLog(@"Sss");
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
