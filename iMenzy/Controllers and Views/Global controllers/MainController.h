//
//  MainController.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  This is a Main controller file with all global control methods used in project
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "Menza.h"


@interface MainController : NSObject

// load remote resources
+(void)loadMenzyResource;
+(void)loadOpeningHoursResource;
+(void)loadJidlaResource;

// Loads XML file from remote URL and save it locally to sandbox as "file" (list of menzy accepted)
+(void)loadXMLFromURL:(NSString *)url menzy:(NSMutableArray *)list toFile:(NSString *)file;

// Loads XML from sandbox "file" and returns dictionary (list of menzy accepted)
+(NSMutableDictionary *)loadXMLFromSandboxFile:(NSString *)file menzy:(NSMutableArray *)list;

// sort menzy Dictionary and return array of Menza objects
+(NSMutableArray *)sortDictionary:(NSMutableDictionary *)dictionary byItem:(NSString *)item orderBy:(int)order;

// PostProcessing menzy to readable format
+(void)postProcessMenzy:(NSMutableArray *)menzyArray;

// PostProcessing opening hours to readable format
+(void)postProcessOpeningHours:(NSMutableArray *)openingHoursArray;

// PostProcessing meals to readable format
+(void)postProcessJidla:(NSMutableArray *)jidlaArray;

// Reachablity - checking internet connection
+(Boolean)isConnectionAvailable;

// HTTP requests
// LINK params
+(NSMutableDictionary *)doRequestToURL:(NSString *)url requestType:(NSString *)type;
// JSON params
+(NSMutableDictionary *)doRequestToURL:(NSString *)url requestType:(NSString *)type withParams:(NSString *)params;

// dates
+(NSString *)dateToString:(NSDate *)date;

// is menza opened?
+(int)isMenzaOpened:(Menza *)menza;

//+ (NSString *)translateText:(NSString*)text source:(NSString *)sourceLanguage destination:(NSString *)destinationLanguage;
@end
