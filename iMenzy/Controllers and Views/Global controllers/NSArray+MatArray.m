//
//  NSArray+MatArray.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Category for sorting NSArrays by other array pattern
//

#import "NSArray+MatArray.h"

@implementation NSArray (MatArray)

static NSInteger comparatorForSortingUsingArray(id object1, id object2, void *context)
{
    NSUInteger index1 = [(__bridge NSArray *)context indexOfObject:object1];
    NSUInteger index2 = [(__bridge NSArray *)context indexOfObject:object2];
    if (index1 < index2)
        return NSOrderedAscending;
    // else
    if (index1 > index2)
        return NSOrderedDescending;
    // else
    return [object1 compare:object2];
}

- (NSArray *)sortedArrayUsingArray:(NSArray *)otherArray
{
    return [self sortedArrayUsingFunction:comparatorForSortingUsingArray context:(__bridge void *)(otherArray)];
}






@end
