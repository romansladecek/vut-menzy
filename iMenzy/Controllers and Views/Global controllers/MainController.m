//
//  MainController.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  This is a Main controller file with all global control methods used in project
//

#import "MainController.h"
#import "XMLReader.h"
#import "SVProgressHUD.h"
#import "Reachability.h"


@interface MainController ()

@end

@implementation MainController


#pragma mark - Load resources
+(void)loadMenzyResource {
    
    // which menzy will be loading
    NSMutableArray *arrayOfMenzy = [NSMutableArray arrayWithObjects:@"VUT", nil];
    
    /*
     * Load MENZY
     *
     */
    NSString *vut_menzy_url = @"http://www.skm.vutbr.cz/app06/datajidelnicek/data2xml.asp?table=ovydejny";
    NSString *vut_menzy_localXMLfile = @"vut_menzy.xml";
    // load XML from URL and save it locally
    [self loadXMLFromURL:vut_menzy_url menzy:arrayOfMenzy toFile:vut_menzy_localXMLfile];
    // load saved XML and assign them to dictionary
    NSMutableDictionary *menzyDictionary = [MainController loadXMLFromSandboxFile:vut_menzy_localXMLfile menzy:arrayOfMenzy];
    // sort menzy and return array of Menza objects
    NSMutableArray *menzyArray = [MainController sortDictionary:menzyDictionary byItem:@"poradi" orderBy:DESC];
    // postprocessing MENZY array and saving them to globally variable (bcs of GPS location mainly)
    [self postProcessMenzy:menzyArray];
    
    //NSLog(@"global array MENZY: %@", global_menzyDict);
    
}

+(void)loadOpeningHoursResource {
    
    // which menzy will be loading
    NSMutableArray *arrayOfMenzy = [NSMutableArray arrayWithObjects:@"VUT", nil];
    
    /*
     * Load OTVARACIE HODINY
     *
     */
    NSString *vut_otevreno_url = @"http://www.skm.vutbr.cz/app06/datajidelnicek/data2xml.asp?table=otevreno";
    NSString *vut_otevreno_localXMLfile = @"vut_otevreno.xml";
    // load XML from URL and save it locally
    [self loadXMLFromURL:vut_otevreno_url menzy:arrayOfMenzy toFile:vut_otevreno_localXMLfile];
    // load saved XML and assign them to dictionary
    NSMutableDictionary *otevrenoDictionary = [MainController loadXMLFromSandboxFile:vut_otevreno_localXMLfile menzy:arrayOfMenzy];
    // sort hours and return array of objects
    NSMutableArray *otevrenoArray = [MainController sortDictionary:otevrenoDictionary byItem:@"id" orderBy:ASC];
    // postprocessing OTVARACIE HODINY array and saving them to globally variable
    [self postProcessOpeningHours:otevrenoArray];
    
    //NSLog(@"global array OTEVRENO: %@", global_otevrenoDict);
    
}

+(void)loadJidlaResource {

    // which menzy will be loading
    NSMutableArray *arrayOfMenzy = [NSMutableArray arrayWithObjects:@"VUT", nil];

    /*
     * Load JIDLA
     *
     */
    NSString *vut_jidla_url = @"http://www.skm.vutbr.cz/app06/datajidelnicek/data2xml.asp?table=seznjidel";
    NSString *vut_jidla_localXMLfile = @"vut_jidla.xml";
    // load XML from URL and save it locally
    [self loadXMLFromURL:vut_jidla_url menzy:arrayOfMenzy toFile:vut_jidla_localXMLfile];
    // load saved XML and assign them to dictionary
    NSMutableDictionary *jidlaDict = [MainController loadXMLFromSandboxFile:vut_jidla_localXMLfile menzy:arrayOfMenzy];
    // sort jidla and return array of objects
    NSMutableArray *jidlaArray= [MainController sortDictionary:jidlaDict byItem:@"id" orderBy:ASC];
    // postprocessing JIDLA dictionary (implicitelly saving them to global variable)
    [self postProcessJidla:jidlaArray];
    
    NSLog(@"loadMeals ended");
    //NSLog(@"global array JIDLA: %@", global_jidlaDict);
    
}

#pragma mark - Working with menzy


// Loads XML file from remote URL and save it locally to sandbox as "file" (list of menzy accepted)
+(void)loadXMLFromURL:(NSString *)url menzy:(NSMutableArray *)list toFile:(NSString *)file {
    

    
    if (![self isConnectionAvailable])
    {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,file];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if ([fileManager fileExistsAtPath:filePath]) {
            [SVProgressHUD showErrorWithStatus:@"Údaje načtené z lokální paměti (možná neaktuální)."];
        }
        else {
            [SVProgressHUD showErrorWithStatus:@"Skontrolujte připojení k internetu před prvním použitím"];
        }
            
        return;
    }
    // VUT
    if ([list containsObject:@"VUT"]) {
        
        // load MENZY - XML
        NSURL * URL = [[NSURL alloc]initWithString:url];
        NSData * data = [[NSData alloc] initWithContentsOfURL:URL];
        
        // save to XML to sandbox for offline access
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
        NSString *documentsDir = [paths objectAtIndex:0];
        NSString *fullPath = [documentsDir stringByAppendingPathComponent:file];
        [data writeToFile:fullPath atomically:YES];
        
    }
}

// Loads XML from sandbox "file" and returns dictionary (list of menzy accepted)
+(NSMutableDictionary *)loadXMLFromSandboxFile:(NSString *)file menzy:(NSMutableArray *)list {
    
    NSMutableDictionary *return_dict = [[NSMutableDictionary alloc] init];

    if (test_version || [file isEqualToString:@"vut_menzy_locations.xml"]) { // offline version with local XMLs using
        

        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *myFile = [mainBundle pathForResource:file ofType:nil];
        
        // VUT
        if ([list containsObject:@"VUT"]) {
            
            NSError *error;
            NSString* contents = [NSString stringWithContentsOfFile:myFile
                                                           encoding:NSWindowsCP1250StringEncoding
                                                              error:&error];
            NSData* xmlData = [contents dataUsingEncoding:NSWindowsCP1250StringEncoding];
            
            
            return_dict = [NSMutableDictionary dictionaryWithDictionary:[XMLReader dictionaryForXMLData:xmlData error:&error]];
            
            

        }
    }
    
    
    else {
    
        
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        // VUT
        if ([list containsObject:@"VUT"]) {

            
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,file];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            if ([fileManager fileExistsAtPath:filePath])
            {
                
                NSError *error;
                NSString* contents = [NSString stringWithContentsOfFile:filePath
                                                               encoding:NSWindowsCP1250StringEncoding
                                                                  error:&error];
                NSData* xmlData = [contents dataUsingEncoding:NSWindowsCP1250StringEncoding];

                
                return_dict = [NSMutableDictionary dictionaryWithDictionary:[XMLReader dictionaryForXMLData:xmlData error:&error]];

                
            }
            else {
                //message = @"No stored XML file locally";

            }
            
        }
    }
    
    return return_dict;
    
}

// sort menzy dictionary and return array of Menza objects (by item, order = ASC/DESC)
+(NSMutableArray *)sortDictionary:(NSMutableDictionary *)dictionary byItem:(NSString *)item orderBy:(int)order {
    
    // working NSMutableArray // specific only for VUT XMLs
    NSArray *working_array = [dictionary[@"rows"] allValues][0];

    // sorting
    NSArray* sortedParsedData = [working_array sortedArrayUsingComparator:(NSComparator)^(NSDictionary *item1, NSDictionary *item2) {
        
        NSComparisonResult returnValue = NSOrderedAscending; // only init
        
            // sorting by ITEM
            NSString *sortedObject1 = item1[item];
            NSString *sortedObject2 = item2[item];
            if ([sortedObject1 isEqual:[NSNull null]]) {
                sortedObject1 = nil;
            }
            else if ([sortedObject2 isEqual:[NSNull null]]) {
                
                sortedObject2 = nil;
            }
        
            // ordering
            if (order == ASC) {
                returnValue = [sortedObject1 compare:sortedObject2 options:NSNumericSearch];
            }
            else { // DESC
                returnValue = [sortedObject2 compare:sortedObject1 options:NSNumericSearch];
            }

        return returnValue;
    }];
    // end of sorting
    
    
    
    working_array = sortedParsedData;
    
    
    return (NSMutableArray *)working_array;

    
    
}

// PostProcessing meals to readable format
+(void)postProcessMenzy:(NSMutableArray *)menzyArray {
    
    
    NSMutableArray *arrayOfMenzy = [NSMutableArray arrayWithObjects:@"VUT", nil];
    
    // get dictionary of locations
    NSString *vut_locations_localXMLfile = @"vut_menzy_locations.xml";
    // load saved XML and assign them to dictionary
    NSMutableDictionary *locationsDictionary = [self loadXMLFromSandboxFile:vut_locations_localXMLfile menzy:arrayOfMenzy];
    // sort menzy and return array of Locations objects
    NSMutableArray *locationsArray = [self sortDictionary:locationsDictionary byItem:@"id" orderBy:ASC];
    NSMutableDictionary *oneLocation = [[NSMutableDictionary alloc] init];
    
    // fill dictionary with new structure (id -> lat, lng)
    [locationsDictionary removeAllObjects];
    
    for (oneLocation in locationsArray) {
        

        NSMutableDictionary *innerDict = [[NSMutableDictionary alloc] init]; // id
        
        [innerDict setObject:oneLocation[@"lat"] forKey:@"lat"];
        [innerDict setObject:oneLocation[@"lng"] forKey:@"lng"];

        [locationsDictionary setObject:innerDict forKey:oneLocation[@"id"]];
    
    }
    
    // adding locations to Menzy global array
    NSMutableDictionary *oneMenza = [[NSMutableDictionary alloc] init];

    NSMutableArray *arrayOfVisibleMenzy = [[NSMutableArray alloc] init];
    NSMutableArray *arrayOfNotVisibleMenzy = [[NSMutableArray alloc] init];

    
    for (oneMenza in menzyArray) {
        if (![oneMenza[@"poradi"] isEqualToString:@"0"]) { // only longterm opened menzy
            [oneMenza setObject:locationsDictionary[oneMenza[@"id"]] forKey:@"location"];
            [global_menzyDict setObject:oneMenza forKey:oneMenza[@"id"]];
            
            // add to NSUserDefault bcs of visibility of menzas
            if (![[NSUserDefaults standardUserDefaults] objectForKey:@"visible_menzy"]) { // if app was started at first time
                
                // visibility of menzy (all are YES at the begining) - same as ordered array
                [arrayOfVisibleMenzy addObject:oneMenza[@"id"]];
                
            }

        }
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"visible_menzy"]) { // if app was started at first time
        
        NSMutableDictionary *outerDict = [[NSMutableDictionary alloc] init]; //== dictOfVisibleMenzy
        [outerDict setObject:arrayOfVisibleMenzy forKey:@"YES"];   // all menzy
        [outerDict setObject:arrayOfNotVisibleMenzy forKey:@"NO"]; // empty

        [[NSUserDefaults standardUserDefaults] setObject:outerDict forKey:@"visible_menzy"];
        [[NSUserDefaults standardUserDefaults] setObject:arrayOfVisibleMenzy forKey:@"order_menzy"];

        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}




// Prosprocessing opening hours to readable format
+(void)postProcessOpeningHours:(NSMutableArray *)openingHoursArray {
    

    // for processing
    NSMutableDictionary *oneDay = [[NSMutableDictionary alloc] init];
    
    // processing global array of opening hours to more readable format
    for (oneDay in openingHoursArray) {
        
        NSMutableDictionary *innerDict = [[NSMutableDictionary alloc] init]; // days
        NSMutableArray *innerInnerArray = [[NSMutableArray alloc] init]; // array of (FROM + TO)'s // if more then one
        NSMutableDictionary *innerInnerInnerDict = [[NSMutableDictionary alloc] init]; // from + to

        // creating date
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"HH:mm"];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [formatter setTimeZone:gmt];
        NSDate *dateFrom = [formatter dateFromString:[NSString stringWithFormat:@"%@:%@", oneDay[@"odH"], oneDay[@"odM"]]];
        NSDate *dateTo = [formatter dateFromString:[NSString stringWithFormat:@"%@:%@", oneDay[@"doH"], oneDay[@"doM"]]];
        [innerInnerInnerDict setObject:dateFrom forKey:@"from"];
        [innerInnerInnerDict setObject:dateTo forKey:@"to"];
        
        // add more
        if ([[global_otevrenoDict allKeys] containsObject:oneDay[@"id"]]) {
            
            // previous dates are save
            innerDict = [global_otevrenoDict objectForKey:oneDay[@"id"]];

            
            if ([[innerDict allKeys] containsObject:oneDay[@"den"]]) {
                // prevous FROM TO's are saved (if more than once is canteen opened in one day)
                innerInnerArray = innerDict[oneDay[@"den"]];
            }

            // and add new one
            [innerInnerArray addObject:innerInnerInnerDict];
            [innerDict setObject:innerInnerArray forKey:oneDay[@"den"]];
            [global_otevrenoDict setObject:innerDict forKey:oneDay[@"id"]];

            
            
        } // add the first one
        else {
            
            // and add new one
            [innerInnerArray addObject:innerInnerInnerDict];
            [innerDict setObject:innerInnerArray forKey:oneDay[@"den"]];
            [global_otevrenoDict setObject:innerDict forKey:oneDay[@"id"]];
        }
        
    }
    
}


// PostProcessing meals to readable format
+(void)postProcessJidla:(NSMutableArray *)jidlaArray {
    
    [global_jidlaDict removeAllObjects];
    
    // for processing
    NSMutableDictionary *oneMeal = [[NSMutableDictionary alloc] init];
    
    // processing global array of opening hours to more readable format
    for (oneMeal in jidlaArray) {
        
        // inner
        NSMutableDictionary *innerDict = [[NSMutableDictionary alloc] init];
        NSMutableArray *innerInnerArray = [[NSMutableArray alloc] init]; // array of meals // if more then one
        //NSLog(@"oneMeal: %@", oneMeal);
        
        // add more
        if ([[global_jidlaDict allKeys] containsObject:oneMeal[@"id"]]) {
        
            // store previous
            innerDict = [global_jidlaDict objectForKey:oneMeal[@"id"]];
            //NSLog(@"inner: %@", innerDict);
            
            if ([[innerDict allKeys] containsObject:oneMeal[@"kategorie"]]) {
                innerInnerArray = innerDict[oneMeal[@"kategorie"]];
            }
                
            [innerInnerArray addObject:oneMeal];
            [innerDict setObject:innerInnerArray forKey:oneMeal[@"kategorie"]];
            [global_jidlaDict setObject:innerDict forKey:oneMeal[@"id"]];

        }
        else {
            [innerInnerArray addObject:oneMeal];
            [innerDict setObject:innerInnerArray forKey:oneMeal[@"kategorie"]];
            [global_jidlaDict setObject:innerDict forKey:oneMeal[@"id"]];
        }
        

    
    }
    
    

    
}


#pragma mark -
#pragma mark Date and Times processing
+(NSString *)dateToString:(NSDate *)date {
    
    NSString *return_string;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH:mm"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [formatter setTimeZone:gmt];
    
    return_string = [formatter stringFromDate:date];
    
    
    return return_string;
    
}

// check if menza is opened now (return number of minutes is opened yet)
+(int)isMenzaOpened:(Menza *)menza {
    
    int isOpened_minutesYet = 0;
    
    // current time
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    NSInteger currentHour = [components hour];
    NSInteger currentMinute = [components minute];
    int minutesFromMidnight_now = currentHour*60 + currentMinute;

    // current day (1-monday, 2-tuesday, ... 7-sunday)
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [calendar setFirstWeekday:2];
    NSUInteger weekday = [calendar ordinalityOfUnit:NSWeekdayCalendarUnit inUnit:NSWeekCalendarUnit forDate:[NSDate date]];
    
    // date formatter
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [formatter setTimeZone:gmt];
    
    
    NSString *day;

    // check today is opened
    if (weekday >= 1 && weekday <= 4) {
        day = @"1";
    }
    else if (weekday == 5) {
        day = @"5";
    }
    else if (weekday == 6) {
        day = @"6";
    }
    else if (weekday == 7) {
        day = @"7";
    }
    
    if (global_otevrenoDict[menza.idd][day]) { // is opened today
        
        int count = [global_otevrenoDict[menza.idd][day] count];
        // all times (more times opened a day)
        for (int i = 0; i < count; i++) {
            
            // creating date
            [formatter setDateFormat:@"HH"];
            int hodiny_from = [[formatter stringFromDate:global_otevrenoDict[menza.idd][day][i][@"from"]] intValue];
            int hodiny_to = [[formatter stringFromDate:global_otevrenoDict[menza.idd][day][i][@"to"]] intValue];
            
            [formatter setDateFormat:@"mm"];
            int minuty_from = [[formatter stringFromDate:global_otevrenoDict[menza.idd][day][i][@"from"]] intValue];
            int minuty_to = [[formatter stringFromDate:global_otevrenoDict[menza.idd][day][i][@"to"]] intValue];
            
            //NSLog(@"NOW: %d:%d", currentHour, currentMinute);
            //NSLog(@"FROM: %d:%d", hodiny_from, minuty_from);
            //NSLog(@"TO: %d:%d", hodiny_to, minuty_to);
            int minutesFromMidnight_from = hodiny_from*60 + minuty_from;
            int minutesFromMidnight_to = hodiny_to*60 + minuty_to;

            if (minutesFromMidnight_now >= minutesFromMidnight_from && minutesFromMidnight_now < minutesFromMidnight_to) {
                isOpened_minutesYet = minutesFromMidnight_to - minutesFromMidnight_now;
            }


            
        }
        
    }

    //NSLog(@"isOpened-MinutesYet: %d", isOpened_minutesYet);

    return isOpened_minutesYet;
    
}

// Reachablity of internet connection
#pragma mark -
#pragma mark Internet Connection Methods

/*
 * Check internet connection
 */
+(Boolean)isConnectionAvailable {
    
    Boolean ret = FALSE;
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [reach currentReachabilityStatus];
    
    if ((internetStatus == ReachableViaWiFi) || (internetStatus == ReachableViaWWAN)){
        ret = TRUE;
    }
    
    return ret;
}




#pragma mark -
#pragma mark HTTP communication
// params passing by url
+(NSMutableDictionary *)doRequestToURL:(NSString *)url requestType:(NSString *)type {
    
    NSString *urlString = [NSString stringWithFormat:@"%@", url];
    NSURL *url_link = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url_link
                                                       cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                   timeoutInterval:60];
	
	[req setHTTPMethod:type];
	[req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
	NSHTTPURLResponse* urlResponse = nil;
	NSError *error = [[NSError alloc] init];
	
    NSData *responseData = [NSURLConnection sendSynchronousRequest:req returningResponse:&urlResponse error:&error];

    
	//NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
    NSDictionary* json_response = [NSJSONSerialization
                                   JSONObjectWithData:responseData
                                   options:kNilOptions
                                   error:&error];

    
    NSMutableDictionary *outputDict = [json_response mutableCopy];
    return outputDict;
    
    
}

// params passing by json input
+(NSMutableDictionary *)doRequestToURL:(NSString *)url requestType:(NSString *)type withParams:(NSString *)params {
 
    NSData *postData = [params dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:type];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLResponse *response = NULL;
    NSError *requestError = NULL;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];

    if (requestError) {
        [SVProgressHUD showErrorWithStatus:@"Failed to read address"];
        return nil;
    }
    
    // parse JSON response to NSDictionary
    NSError* error;
    NSDictionary* json_response = [NSJSONSerialization
                                   JSONObjectWithData:responseData
                                   options:kNilOptions
                                   error:&error];
    
    NSMutableDictionary *outputDict = [json_response mutableCopy];

    return outputDict;
    
}


@end
