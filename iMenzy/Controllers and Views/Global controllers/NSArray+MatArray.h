//
//  NSArray+MatArray.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Category for sorting NSArrays by other array pattern
//

#import <Foundation/Foundation.h>

@interface NSArray (MatArray)

- (NSArray *)sortedArrayUsingArray:(NSArray *)otherArray;

@end
