//
//  MapAndDirectionView.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View Maps and direction
//

#import "MapAndDirectionView.h"
#import "ZoznamMenzViewController.h"

@implementation MapAndDirectionView

- (id)initWithParent:(id)parent
{
    self = [super init];
    if (self) {
        
        self.parentVC = parent;
        
        self.backgroundColor = [UIColor clearColor];
        
        self.lbl_distance = [[UILabel alloc] init];
        self.lbl_distance.frame = CGRectMake(0, 0, 270, 40);
        self.lbl_distance.numberOfLines = 1;
        self.lbl_distance.textAlignment = NSTextAlignmentCenter;
        self.lbl_distance.font = [UIFont fontWithName:DEF_FONT size:24.0];
        self.lbl_distance.textColor = [UIColor whiteColor];
        self.lbl_distance.text = @"250 m";
        self.lbl_distance.backgroundColor = [UIColor clearColor];
        [self addSubview:self.lbl_distance];

        self.img_compass = [[UIImageView alloc] init];
        self.img_compass.frame = CGRectMake(80, 50, 100, 100);
        self.img_compass.backgroundColor = [UIColor clearColor];
        self.img_compass.image = [UIImage imageNamed:@"compass_arrow.png"];
        [self addSubview:self.img_compass];
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        //[self.locationManager startUpdatingLocation];
        //[self.locationManager startUpdatingHeading];
        
        CLLocation *location = [self.locationManager location];
        CLLocationCoordinate2D user = [location coordinate];
        
        [self calculateUserAngle:user];
        
        
    }
    return self;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    CLLocationCoordinate2D here =  newLocation.coordinate;
    
    [self calculateUserAngle:here];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    //self.img_compass.transform = CGAffineTransformMakeRotation(newHeading.magneticHeading * M_PI / 180);
    self.img_compass.transform = CGAffineTransformMakeRotation((degrees - newHeading.magneticHeading) * M_PI / 180);
}

-(void) calculateUserAngle:(CLLocationCoordinate2D)user {
    locLat = [global_menzyDict[[self.parentVC id_selected_menzy]][@"location"][@"lat"] floatValue];
    locLon = [global_menzyDict[[self.parentVC id_selected_menzy]][@"location"][@"lng"] floatValue];
    
    // meters from here
    // menza location
    //float menza_location_lat = [global_menzyDict[menza.idd][@"location"][@"lat"] floatValue];
    //float menza_location_lng = [global_menzyDict[menza.idd][@"location"][@"lng"] floatValue];
    
    CLLocation *menza_location = [[CLLocation alloc] initWithLatitude:locLat longitude:locLon];
    
    
    CLLocation *my_location;// = [self.parentVC deviceLocation];
    //NSLog(@"my location: %f, %f", my_location.coordinate.latitude, my_location.coordinate.longitude);
    
    CLLocationDistance distance = [my_location distanceFromLocation:menza_location];
    if (distance < 1000) {
        self.lbl_distance.text = [NSString stringWithFormat:@"%d m", (int)distance];
    }
    else {
        self.lbl_distance.text = [NSString stringWithFormat:@"%d km %d m", (int)distance/1000, (int)fmod(distance, 1000)];
    }
    
    /////////
    
    NSLog(@"%f ; %f, myloc: %f, %f,  distance: %f", locLat, locLon, my_location.coordinate.latitude, my_location.coordinate.latitude, distance);
    
    float pLat = 0;
    float pLon = 0;
    
    if(locLat > user.latitude && locLon > user.longitude) {
        // north east
        
        pLat = user.latitude;
        pLon = locLon;
        
        degrees = 0;
    }
    else if(locLat > user.latitude && locLon < user.longitude) {
        // south east
        
        pLat = locLat;
        pLon = user.longitude;
        
        degrees = 45;
    }
    else if(locLat < user.latitude && locLon < user.longitude) {
        // south west
        
        pLat = locLat;
        pLon = user.latitude;
        
        degrees = 180;
    }
    else if(locLat < user.latitude && locLon > user.longitude) {
        // north west
        
        pLat = locLat;
        pLon = user.longitude;
        
        degrees = 225;
    }
    
    // Vector QP (from user to point)
    float vQPlat = pLat - user.latitude;
    float vQPlon = pLon - user.longitude;
    
    // Vector QL (from user to location)
    float vQLlat = locLat - user.latitude;
    float vQLlon = locLon - user.longitude;
    
    // degrees between QP and QL
    float cosDegrees = (vQPlat * vQLlat + vQPlon * vQLlon) / sqrt((vQPlat*vQPlat + vQPlon*vQPlon) * (vQLlat*vQLlat + vQLlon*vQLlon));
    degrees = degrees + acos(cosDegrees);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
