//
//  MapAndDirectionView.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View Settings
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapAndDirectionView : UIView <CLLocationManagerDelegate>
{
    
    int degrees;
    float locLat, locLon;
}

@property (nonatomic, retain) id parentVC;

@property (nonatomic, retain) UILabel *lbl_distance;
@property (nonatomic, retain) UIImageView *img_compass;

@property (nonatomic, retain) CLLocationManager *locationManager;

- (id)initWithParent:(id)parent;

@end
