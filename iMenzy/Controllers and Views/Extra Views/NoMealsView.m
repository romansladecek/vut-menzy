//
//  NoMealsView.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View No Meals
//

#import "NoMealsView.h"

@implementation NoMealsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.backgroundColor = [UIColor clearColor];
        
        self.lbl_status = [[UILabel alloc] init];
        self.lbl_status.frame = CGRectMake(0, 100, 260, 40);
        self.lbl_status.numberOfLines = 1;
        self.lbl_status.textAlignment = NSTextAlignmentCenter;
        self.lbl_status.font = [UIFont fontWithName:DEF_FONT size:16.0];
        self.lbl_status.textColor = [UIColor whiteColor];
        self.lbl_status.backgroundColor = [UIColor clearColor];
        [self addSubview:self.lbl_status];
    
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
