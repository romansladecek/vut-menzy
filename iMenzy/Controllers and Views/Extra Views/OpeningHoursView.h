//
//  OpeningHoursView.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View Opening hours
//

#import <UIKit/UIKit.h>

@interface OpeningHoursView : UIView

@property (nonatomic, retain) UILabel *lbl_monday;
@property (nonatomic, retain) UILabel *lbl_friday;
@property (nonatomic, retain) UILabel *lbl_saturday;
@property (nonatomic, retain) UILabel *lbl_sunday;


@property (nonatomic, retain) UILabel *lbl_monday_time;
@property (nonatomic, retain) UILabel *lbl_friday_time;
@property (nonatomic, retain) UILabel *lbl_saturday_time;
@property (nonatomic, retain) UILabel *lbl_sunday_time;


@end
