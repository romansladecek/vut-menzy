//
//  OpeningHoursView.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View Opening hours
//

#import "OpeningHoursView.h"

@implementation OpeningHoursView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.backgroundColor = [UIColor clearColor];
        
        self.lbl_monday = [[UILabel alloc] init];
        self.lbl_monday.frame = CGRectMake(0, 0, 120, 30);
        self.lbl_monday.numberOfLines = 1;
        self.lbl_monday.textAlignment = NSTextAlignmentRight;
        self.lbl_monday.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_monday.textColor = [UIColor whiteColor];
        self.lbl_monday.backgroundColor = [UIColor clearColor];
        self.lbl_monday.text = @"Pondelí - Čtvrtek";
        [self addSubview:self.lbl_monday];
        
        self.lbl_friday = [[UILabel alloc] init];
        self.lbl_friday.frame = CGRectMake(0, 40, 120, 30);
        self.lbl_friday.numberOfLines = 1;
        self.lbl_friday.textAlignment = NSTextAlignmentRight;
        self.lbl_friday.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_friday.textColor = [UIColor whiteColor];
        self.lbl_friday.backgroundColor = [UIColor clearColor];
        self.lbl_friday.text = @"Pátek";
        [self addSubview:self.lbl_friday];
        
        self.lbl_saturday = [[UILabel alloc] init];
        self.lbl_saturday.frame = CGRectMake(0, 80, 120, 30);
        self.lbl_saturday.numberOfLines = 1;
        self.lbl_saturday.textAlignment = NSTextAlignmentRight;
        self.lbl_saturday.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_saturday.textColor = [UIColor whiteColor];
        self.lbl_saturday.backgroundColor = [UIColor clearColor];
        self.lbl_saturday.text = @"Sobota";
        [self addSubview:self.lbl_saturday];
        
        self.lbl_sunday = [[UILabel alloc] init];
        self.lbl_sunday.frame = CGRectMake(0, 120, 120, 30);
        self.lbl_sunday.numberOfLines = 1;
        self.lbl_sunday.textAlignment = NSTextAlignmentRight;
        self.lbl_sunday.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_sunday.textColor = [UIColor whiteColor];
        self.lbl_sunday.backgroundColor = [UIColor clearColor];
        self.lbl_sunday.text = @"Neděle";
        [self addSubview:self.lbl_sunday];
        
        
        // times
        self.lbl_monday_time = [[UILabel alloc] init];
        self.lbl_monday_time.frame = CGRectMake(130, 0, 120, 30);
        self.lbl_monday_time.numberOfLines = 2;
        self.lbl_monday_time.textAlignment = NSTextAlignmentLeft;
        self.lbl_monday_time.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_monday_time.textColor = [UIColor whiteColor];
        self.lbl_monday_time.backgroundColor = [UIColor clearColor];
        self.lbl_monday_time.text = @"14:00 - 19:00\n20:00 - 23:00";
        [self addSubview:self.lbl_monday_time];
        
        self.lbl_friday_time = [[UILabel alloc] init];
        self.lbl_friday_time.frame = CGRectMake(130, 40, 120, 30);
        self.lbl_friday_time.numberOfLines = 1;
        self.lbl_friday_time.textAlignment = NSTextAlignmentLeft;
        self.lbl_friday_time.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_friday_time.textColor = [UIColor whiteColor];
        self.lbl_friday_time.backgroundColor = [UIColor clearColor];
        self.lbl_friday_time.text = @"14:00 - 19:00";
        [self addSubview:self.lbl_friday_time];
        
        self.lbl_saturday_time = [[UILabel alloc] init];
        self.lbl_saturday_time.frame = CGRectMake(130, 80, 120, 30);
        self.lbl_saturday_time.numberOfLines = 1;
        self.lbl_saturday_time.textAlignment = NSTextAlignmentLeft;
        self.lbl_saturday_time.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_saturday_time.textColor = [UIColor whiteColor];
        self.lbl_saturday_time.backgroundColor = [UIColor clearColor];
        self.lbl_saturday_time.text = @"14:00 - 19:00";
        [self addSubview:self.lbl_saturday_time];
        
        self.lbl_sunday_time = [[UILabel alloc] init];
        self.lbl_sunday_time.frame = CGRectMake(130, 120, 120, 30);
        self.lbl_sunday_time.numberOfLines = 1;
        self.lbl_sunday_time.textAlignment = NSTextAlignmentLeft;
        self.lbl_sunday_time.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_sunday_time.textColor = [UIColor whiteColor];
        self.lbl_sunday_time.backgroundColor = [UIColor clearColor];
        self.lbl_sunday_time.text = @"14:00 - 19:00";
        [self addSubview:self.lbl_sunday_time];
    
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
