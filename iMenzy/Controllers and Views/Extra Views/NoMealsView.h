//
//  NoMealsView.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View No Meals
//

#import <UIKit/UIKit.h>

@interface NoMealsView : UIView

@property (nonatomic, retain) UILabel *lbl_status;


@end
