//
//  SettingsView.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View Settings
//

#import <UIKit/UIKit.h>
#import "AKSSegmentedSliderControl.h"

@interface SettingsView : UIView <AKSSegmentedSliderControlDelegate>

@property (nonatomic, retain) id parentVC;

@property (nonatomic, retain) UILabel *lbl_price_title;
@property (nonatomic, retain) UILabel *lbl_price_student;
@property (nonatomic, retain) UILabel *lbl_price_employee;
@property (nonatomic, retain) UILabel *lbl_price_others;

@property (nonatomic, retain) AKSSegmentedSliderControl *slider_prices;


- (id)initWithParent:(id)parent;

@end
