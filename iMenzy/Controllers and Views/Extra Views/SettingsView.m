//
//  SettingsView.m
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  View Settings
//

#import "SettingsView.h"
#import "ZoznamMenzViewController.h"
#import "RNBlurModalView.h"
#import <AudioToolbox/AudioToolbox.h>

#define WIDTH  282
#define HEIGHT 34
#define X_POS  55
#define Y_POS  40
#define RADIUS_POINT  10
#define SPACE_BETWEEN_POINTS  44.75
#define SLIDER_LINE_WIDTH     9
#define IPHONE_4_SUPPORT      88

@implementation SettingsView

- (id)initWithParent:(id)parent
{
    self = [super init];
    if (self) {
        
        self.parentVC = parent;

        self.backgroundColor = [UIColor clearColor];
        
        self.lbl_price_title = [[UILabel alloc] init];
        self.lbl_price_title.frame = CGRectMake(0, 0, 260, 40);
        self.lbl_price_title.numberOfLines = 1;
        self.lbl_price_title.textAlignment = NSTextAlignmentCenter;
        self.lbl_price_title.font = [UIFont fontWithName:DEF_FONT size:16.0];
        self.lbl_price_title.textColor = [UIColor whiteColor];
        self.lbl_price_title.text = @"Zobrazení ceny:";
        self.lbl_price_title.backgroundColor = [UIColor clearColor];
        [self addSubview:self.lbl_price_title];

        self.lbl_price_student = [[UILabel alloc] init];
        self.lbl_price_student.frame = CGRectMake(20, 60, 80, 40);
        self.lbl_price_student.numberOfLines = 1;
        self.lbl_price_student.textAlignment = NSTextAlignmentCenter;
        self.lbl_price_student.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_price_student.textColor = [UIColor whiteColor];
        self.lbl_price_student.text = @"Student";
        self.lbl_price_student.backgroundColor = [UIColor clearColor];
        [self addSubview:self.lbl_price_student];
        
        self.lbl_price_employee = [[UILabel alloc] init];
        self.lbl_price_employee.frame = CGRectMake(90, 60, 80, 40);
        self.lbl_price_employee.numberOfLines = 1;
        self.lbl_price_employee.textAlignment = NSTextAlignmentCenter;
        self.lbl_price_employee.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_price_employee.textColor = [UIColor whiteColor];
        self.lbl_price_employee.text = @"Zaměstnanec";
        self.lbl_price_employee.backgroundColor = [UIColor clearColor];
        [self addSubview:self.lbl_price_employee];
        
        self.lbl_price_others = [[UILabel alloc] init];
        self.lbl_price_others.frame = CGRectMake(160, 60, 80, 40);
        self.lbl_price_others.numberOfLines = 1;
        self.lbl_price_others.textAlignment = NSTextAlignmentCenter;
        self.lbl_price_others.font = [UIFont fontWithName:DEF_FONT size:12.0];
        self.lbl_price_others.textColor = [UIColor whiteColor];
        self.lbl_price_others.text = @"Ostatní";
        self.lbl_price_others.backgroundColor = [UIColor clearColor];
        [self addSubview:self.lbl_price_others];
        
        [self prepareSlider];

        
        // info button
        UIButton *btn_info = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_info.frame = CGRectMake(110, 120, 44, 44);
        [btn_info addTarget:self
                              action:@selector(showInfo)
                    forControlEvents:UIControlEventTouchUpInside];
        [btn_info setBackgroundImage:[UIImage imageNamed:@"info_button"] forState:UIControlStateNormal];
        [self addSubview:btn_info];
        
        

    }
    return self;
}

// modal black view
-(void)showInfo {
    
    RNBlurModalView *modal = [[RNBlurModalView alloc] initWithViewController:self.parentVC title:@"VUT Menzy v1.0"
                                    message:@"Created by Roman Sládeček\n \
                                            sladecek.roman@gmail.com\n2013"];
    [modal show];
    
}


-(void)prepareSlider{
    
	CGRect sliderConrolFrame = CGRectNull;
    
	sliderConrolFrame = CGRectMake(X_POS,Y_POS+6.5,WIDTH,HEIGHT);
    
    self.slider_prices = [[AKSSegmentedSliderControl alloc] initWithFrame:sliderConrolFrame];
    [self.slider_prices setNumberOfPoints:3];
    [self.slider_prices setDelegate:self];
	[self.slider_prices moveToIndex:[[[NSUserDefaults standardUserDefaults] objectForKey:@"price_displayed"] intValue]];
    [self.slider_prices setStrokeColor:[UIColor clearColor]];
	[self.slider_prices setSpaceBetweenPoints:SPACE_BETWEEN_POINTS];
	[self.slider_prices setRadiusPoint:RADIUS_POINT];
	[self.slider_prices setHeightLine:SLIDER_LINE_WIDTH];
    [self addSubview:self.slider_prices];
    
}

// slider has changed
- (void)timeSlider:(AKSSegmentedSliderControl *)timeSlider didSelectPointAtIndex:(int)index{
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:index] forKey:@"price_displayed"];
    [[self.parentVC table_jedla] reloadData];
    
}

// easter egg - MNAAAU
- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {

    if (motion == UIEventSubtypeMotionShake && [self.parentVC editModeON] && self.slider_prices.currentIndex == 2) {
        
        CFBundleRef mainBundle = CFBundleGetMainBundle();
        CFURLRef soundFileURLRef;
        soundFileURLRef = CFBundleCopyResourceURL(mainBundle, (CFStringRef) @"mnau", CFSTR ("mp3"), NULL);
        
        UInt32 soundID;
        AudioServicesCreateSystemSoundID(soundFileURLRef, &soundID);
        AudioServicesPlaySystemSound(soundID);
        NSLog(@"shaked");
        
    }
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
