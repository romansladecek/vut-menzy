//
//  ZoznamMenzViewController.h
//  iMenzy
//
//  Created by Roman Sladecek on 1/11/12.
//  Copyright (c) 2013 Roman Sladecek. All rights reserved.
//
//  Main controller for app - table of meals and menzy
//

#import <UIKit/UIKit.h>
#import "MealCategoriesScrollView.h"
#import "LXReorderableCollectionViewFlowLayout.h"
#import "OpeningHoursView.h"
#import "THCircularProgressView.h"
#import "NoMealsView.h"
#import "SettingsView.h"
//#import "MapAndDirectionView.h"

@interface ZoznamMenzViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, LXReorderableCollectionViewDataSource,LXReorderableCollectionViewDelegateFlowLayout>

// top bar
@property (nonatomic, retain) UILabel *lbl_main_KaM_title;

@property (nonatomic, retain) UIButton *btn_settings;

// opening hours view
@property (nonatomic, retain) UIView *view_opening_hours;
@property (nonatomic, retain) UIButton *btn_opening_hours;
@property (nonatomic, retain) UIButton *btn_opening_hours_hand;



//@property (nonatomic, retain) UIView *view_map;
//@property (nonatomic, retain) UIButton *btn_map;
//@property (nonatomic, retain) UIButton *btn_map_spinner;


@property (nonatomic, retain) UILabel *lbl_title;
@property (assign, readwrite) BOOL editModeON;
//@property (assign, readwrite) BOOL mapModeON;
@property (assign, readwrite) BOOL clockModeON;

// refresh circle view
@property (nonatomic, retain) THCircularProgressView *progressCircleView;
@property (nonatomic, strong) NSTimer *timer_progressCircleView;
@property (nonatomic) CGFloat percentage_progressCircleView;
@property (nonatomic, retain) UIButton *btn_refreshAll;



@property (nonatomic, retain) LXReorderableCollectionViewFlowLayout *layout;

@property (nonatomic, retain) UICollectionView *menzy_collectionView;
@property (nonatomic, retain) MealCategoriesScrollView *scroll_meal_categories;

@property (nonatomic, retain) NSString *id_selected_menzy;
@property (nonatomic, retain) NSString *id_selected_category;
@property (nonatomic, retain) UITableView *table_jedla;

// Opening hours
@property (nonatomic, retain) OpeningHoursView* openingHoursView;
// noMeals view
@property (nonatomic, retain) NoMealsView* noMealsView;
@property (assign, readwrite) float backup_alpha_noMealsView;
// Settings View
@property (nonatomic, retain) SettingsView* settingsView;
// Map View
//@property (nonatomic, retain) MapAndDirectionView* mapAndDirectionView;

// Location
//@property (nonatomic, retain) CLLocationManager *locationManager;

//-(CLLocation *)deviceLocation;
-(void)mealCategoryPressed:(id)sender;

@end
